---
title: "Servidor Jitsi"
icon: fa-solid fa-microphone-lines-slash
linkTitle: "Jitsi"
weight: 5
date: 2020-06-03
description: >
  Instalación y estadísticas
categories: [Multimedia]
tags: [DIY,video]
---

## LOGS

<https://wiki.archlinux.org/index.php/Jitsi-meet#Log_evaluation>

```Bash
tail -f /var/log/jitsi/j*.log /var/log/prosody/prosody.{err,log} | grep -v AbstractHealthCheckService
```

Ver salas creadas y cerradas:

```Bash
grep -E 'Created|Disposed' /var/log/jitsi/jicofo.log | cut -d" " -f2,3,7,11,14
```

Las IPs aparecen en /var/log/jitsi/jvb.log

```Bash
journalctl --unit jicofo.service | grep "Created new focus" | cut -d" " -f7,8,16

journalctl --unit jicofo.service | grep "Disposed conference for room" | cut -d" " -f7,8,16
```

## ESTADÍSTICAS

<https://github.com/jitsi/jitsi-videobridge/blob/master/doc/statistics.md>

<https://github.com/jitsi/jitsi-videobridge/blob/master/doc/rest.md>

<https://www.callstats.io/integrate-jitsi/>

<https://www.callstats.io/integrate/jitsi-videobridge/>

<https://community.jitsi.org/t/monitoring-of-jitsi-meet/28226/19>

<https://community.jitsi.org/t/command-or-log-file-for-jitsi-server-status/27722>

<https://github.com/haidlir/jitsi-monitoring>