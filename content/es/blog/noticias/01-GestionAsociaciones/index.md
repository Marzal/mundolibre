---
date: 2022-06-04
title: "Aplicaciones para gestion de Asociaciones State of the Art"
linkTitle: "Asociaciones State of the Art"
description: "Investigación sobre las opciones disponibles y sus caracterisiticas limitaciones"
author: David Marzal ([@DavidMarzalC](https://masto.es/@DavidMarzalC))
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "params.byline"
---

## Requisitos

 Cuotas, Socios, proveedores, clientes, Control remesas bancarias, subvenciones, stakeholders,
Gracias a Julian Moyano



## Resumen
|     Aplicacion                          | Última version      | Código Fuente                          | Demo                            | Carencias         | Info |
|-----------------------------------------|---------------------|----------------------------------------|---------------------------------|-------------------|-----------|
|[GONG](https://gong.es)                  | 2021-10-15 (4.07) |[2022-06-02](https://git.semillasl.com/gong/gor)| |  |  | Enfocado a proyectos?

### CRM
|     Aplicacion                          | Última version      | Código Fuente                          | Demo                            | Carencias         | Info |
|-----------------------------------------|---------------------|----------------------------------------|---------------------------------|-------------------|-----------|
|[CiviCRM](https://civicrm.org/)          | 2022-06-02 (5.50.1) |[2022-06-04](https://github.com/civicrm)|[Demo](https://civicrm.org/demo) |  |  |
|[]()      | 2022-06-02 () |[2022-06-04]()|[Demo]() |  |  |
|[SuiteCRM](https://suitecrm.com/)        | 2022-05-24 (8.1.1) |[2022-05-22](https://github.com/salesagility/SuiteCRM)|[Demo](https://suitecrm.com/demo/) |  |  | 
|[VTiger](https://www.vtiger.com/open-source-crm/)| 2022-01-18 (7.4.0h1) |[2022-06-03](https://code.vtiger.com/vtiger/vtigercrm)|[Demo](https://www.vtiger.com/crm-demo/) |  |  | 

### ERP
|     Aplicacion                          | Última version      | Código Fuente                          | Demo                            | Carencias         | Info |
|-----------------------------------------|---------------------|----------------------------------------|---------------------------------|-------------------|-----------|
|[dolibarr](https://www.dolibarr.es/)     | 2022-05-17 (15.0.2) |[2022-06-04](https://github.com/Dolibarr/dolibarr)|[Demo](https://www.dolibarr.es/index.php/dolicloud) |  | __ES__ - Tambien CRM | 
|[Eneboo](https://eneboo.org)             | 2022-04-06 ([2.5.7](https://eneboo.org/pub/contrib/releases/)) |[2022-04](https://github.com/eneboo)| - |  | __ES__ - |
|[AbanQ](https://www.abanq.es/)           | __2014-01__ (2.5) |[2013-06-26](https://github.com/falbujer/AbanQ)| |  | __ES__ - Fork: [Eneboo](https://es.wikipedia.org/wiki/Abanq) | 
|[iDempiere](https://www.idempiere.org/)  | 2021-12-06 (8.2) |[2022-06-02](https://github.com/idempiere/idempiere)|[Demo](https://www.idempiere.org/test-sites/) |  | OSGI + Adempiere - Docker |
|[ADempiere](http://www.adempiere.io/)    | __2019-12-02__ (3.9.3) |[2022-06-02](https://github.com/adempiere/adempiere)|[Demo](https://adempiere.io/web/guest/demo) |  | __ES__ - Es un [fork de Compiere](https://en.wikipedia.org/wiki/Adempiere) | 
|[ERPNext](https://erpnext.com/)          | 2022-05-31 (13.32) |[2022-06-02](https://github.com/frappe/erpnext)|[Demo](https://searx.webheberg.info/search?q=ERPNext+demo) |  |  |
|[Tryton](https://www.tryton.org/  )      | 2022-05-22 (6.4) |[2022-05-02](https://hg.tryton.org/)|[Demo](https://www.tryton.org/download) |  | Docker | 
|[LedgerSMB](https://ledgersmb.org/)      | 2022-04-29 (1.9.14) |[2022-06-04](https://github.com/ledgersmb/LedgerSMB)|[Demo](https://ledgersmb.org/content/demo) |  |  |
|[Apache OFBiz](https://ofbiz.apache.org/)| 2022-06-02 () |[2022-06-02](https://github.com/apache/ofbiz-framework)|[Demo](https://ofbiz.apache.org/ofbiz-demos.html) |  |  |
|[Odoo](https://www.odoo.com)             | 2022-06-02 (15) |[2022-06-01](https://github.com/odoo/odoo)| | [Contabilidad no FLOSS](https://www.odoo.com/es_ES/page/editions) | __ES__ -  |

### Licencia?
|     Aplicacion                          | Última version      | Código Fuente                          | Demo                            | Carencias         | Info |
|-----------------------------------------|---------------------|----------------------------------------|---------------------------------|-------------------|-----------|
|[sinergiacrm](https://sinergiacrm.org)   |  |No parece FLOSS|[Demo](https://opendemo.sinergiacrm.org/) |  |  |

### Ya no son FLOSS
|     Aplicacion                          | Última version      | Código Fuente                          | Demo                            | Carencias         | Info |
|-----------------------------------------|---------------------|----------------------------------------|---------------------------------|-------------------|-----------|
|[OpenBravo](https://www.openbravo.com)   | 2021-03-23 (Q1-2021) |[2022-06-04](https://gitlab.com/openbravo)|  |  | [2020 Fin CE](https://www.openbravo.com/blog/openbravo-to-end-community-edition-open-source-projects-2020/) |
|[SugarCRM](https://www.sugarcrm.com/)    |  |[Ya no es Open Source](https://en.wikipedia.org/wiki/SugarCRM)|  |  |  | Fork: SuiteCRM/SpiceCRM/SarvCRM/Vtiger

### Muertas
* 
  * Philanthros ERP (basado en OpenBravo)
  * Philanthros CRM (basado en VTiger)
* [Epesi](https://en.wikipedia.org/wiki/Epesi)
* GNU Enterprise (GNUe)
* [ERP5](https://en.wikipedia.org/wiki/ERP5) - 2014 - [SOURCE](https://lab.nexedi.com/nexedi/erp5)

## Alternativas
* https://en.wikipedia.org/wiki/Comparison_of_accounting_software
* https://en.wikipedia.org/wiki/List_of_ERP_software_packages
* https://en.wikipedia.org/wiki/Comparison_of_CRM_systems
