---
date: 2025-01-27
title: "El Fediverso es divertido"
linkTitle: "FediListas"
description: "Starter Pack para el Fediverso"
author: David Marzal ([@DavidMarzalC](https://masto.es/@DavidMarzalC))
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    #byline: "David Marzal"
---

## Contexto
Fruto de la iniciativa [Vámonos juntas](https://vamonosjuntas.org) y de la "[enmierdificación](https://es.wikipedia.org/wiki/Decadencia_de_plataformas)" de algunas redes sociales, se ha producido un movimiento de colectivos, personas e incluso empresas de redes privativas y toxicas hacia el [Fediverso](https://es.wikipedia.org/wiki/Fediverso).

Al no estar dirigido por algoritmos ni intereses comerciales, al abrir una cuenta no veremos nada y tenemos que empezar a seguir a gente para rellenar nuestro "timeline/feed/muro/linea de tiempo". Es por eso que hay webs que te muestran listados de gente, pero el proceso no es suficientemente ágil.

Una alternativa es usar la opción de las [plataformas del Fediverso](https://graph.org/lamagiadelofederado-06-12) como Mastodon/Sharkey de importar [cuentas](https://vamonosjuntas.org/tips/?instancia=mastodon.social)(lista de seguidos) o Listas.

## Listados (Listas)

* Ventajas de importar un listado de tipo __Listas__:
  * Sigues a mucha gente de golpe y siempre las puedes dejar de seguir si no te interesan nada (en Mastodon)
  * Tienes organizadas las personas que has seguido importando (en Misskey/*key creas la lista, pero no sigues las cuentas)
  * Puedes configurarla para que no te aparezca lo que esas cuentas publiquen y solo ver que se cuentan entrado al listado específicamente.
* Podéis seguir las novedades o solicitar inclusiones usando las etiquetas de cada listado o con la general #VamosJuntasListados

### Selección personal
|  Tipo   |               Descripción             |  Enlace   | Visualizar | Cambios | Etiquetas
|---------|:--------------------------------------|:---------:|:----------:|:-------:|----------
| Lista   | Podcasting en Español                 | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_Podcast.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_Podcast.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_Podcast.csv?ref_type=heads) | #VamosJuntasDePodcasting #VamosJuntasListados
| Lista   | Accesibilidad                         | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_Accesibilidad.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_Accesibilidad.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_Accesibilidad.csv?ref_type=heads) | #VamosJuntasA11y #VamosJuntasListados
| Lista   | Medio Ambiente + Activismos           | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_BetterPlanet.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_BetterPlanet.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_BetterPlanet.csv?ref_type=heads) | #VamosJuntasSostenibilidad #VamosJuntasListados
| Lista   | Veganismo / AntiEspecismo / Animalismo | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_Vegan.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_Vegan.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_Vegan.csv?ref_type=heads) | #VamosJuntasPorLosAnimales #VamosJuntasListados
| _Cuentas_ | Misma temática pero compilación de [Mi Universo verde](https://miuniversoverde.com/contacto/) | [Descarga](https://miuniversoverde.com/wp-content/uploads/2025/03/mastodon-starter-pack-final-veganismo-antiespecismo.csv) | Añade al anterior cuentas en otros idiomas | Importar en "Lista de seguidos" | #VamosJuntasMiUniversoVerde #MiUniversoVerde
| Lista   | Software y Cultura libre  / (FLOSS)   | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_FLOSS.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_FLOSS.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_FLOSS.csv?ref_type=heads) | #VamosJuntasFLOSS #VamosJuntasListados
| Lista   | Hardware Libre, Makers, 3D, Dispositivos | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_Hardware.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_Hardware.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_Hardware.csv?ref_type=heads) | #VamosJuntasHW #VamosJuntasListados
| Lista   | Medios de Comunicacion                | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_MediosComunicacion.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_MediosComunicacion.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_MediosComunicacion.csv?ref_type=heads) | #VamosJuntasMedios #VamosJuntasListados
| Lista   | Gente de la Región de Murcia          | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_RMurcia.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_RMurcia.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_RMurcia.csv?ref_type=heads) | #VamosJuntasRM #VamosJuntasListados
| Lista   | Unión Europea (in English mayormente) | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_DavidMarzalC_EuropeanUnion.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_DavidMarzalC_EuropeanUnion.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_DavidMarzalC_EuropeanUnion.csv?ref_type=heads) | #VamosJuntasUE #VamosJuntasListados
| _Cuentas_ | Personas/proyectos de VamonosJuntas | [Descarga](https://vamonosjuntas.org/following_accounts.csv) |[Contenido](https://codeberg.org/kyva/vamonos-juntas/src/branch/main/public/following_accounts.csv) | [Historial](https://codeberg.org/kyva/vamonos-juntas/commits/branch/main/public/following_accounts.csv) | #VamonosJuntas #Juntas

### KDE
|  Tipo   |     Relacionados con KDE             |  Enlace   | Visualizar | Cambios | Etiquetas
|---------|:-------------------------------------|:---------:|:----------:|:-------:|----------
| Lista   | Usuarios de KDE por el Fediverso     | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_KDE_Komunidad.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_KDE_Komunidad.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_KDE_Komunidad.csv?ref_type=heads) | #VamosJuntasKomunidad #VamosJuntasListados
| Lista   | Gente o proyectos implicadas en KDE  | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_KDE_KDE.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_KDE_KDE.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_KDE_KDE.csv?ref_type=heads) | #VamosJuntasKDE_es #VamosJuntasListados
| Lista   | Cuentas que suelen promocionar KDE   | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_KDE_Colaboradores.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_KDE_Colaboradores.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_KDE_Colaboradores.csv?ref_type=heads) | #VamosJuntasKDE_dif #VamosJuntasListados
| Lista   | KDE in English (devs y proyectos)    | [Descarga](https://kdeexpress.gitlab.io/fedi/mastodonL_KDE_KDE_int.csv) | [Contenido](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/blob/main/static/fedi/mastodonL_KDE_KDE_int.csv?ref_type=heads) | [Historial](https://gitlab.com/kdeexpress/kdeexpress.gitlab.io/-/commits/main/static/fedi/mastodonL_KDE_KDE_int.csv?ref_type=heads) | #VamosJuntasKDE_int #VamosJuntasListados

## Tutorial
{{% alert title="Importante" color="info" %}}
Puedes repetir este proceso las veces que quieras (descargando de nuevo el fichero) y obtendrás las nuevas incorporaciones al listado. O ver manualmente los cambios de [cada lista](#listados-listas).
{{% /alert %}}

Tras descargar el [Listado](#listados-listas) (como los de la sección anterior):

* Mastodon
  * Ir a __Preferencias__ -> Importar y Exportar -> Importar
  * Seleccionar : Importar tipo -> Listas
  * Pulsar en __Examinar__ y seleccionar el fichero CSV descargado
  * Dejar seleccionado: __Unir__ (Mantener registros existentes y añadir nuevos)
  * Pulsar __Cargar__
{{% imgproc MastodonImport Fit "522x442" webp %}}
Ejemplo del panel de configuración en Mastodon
{{% /imgproc %}}
* Sharkey/Misskey
  * Ir a __Configuración__ -> Importar y Exportar
  * En __Listas__ -> Importar
  * Pulsar el botón __Importar__ -> Subir y seleccionar el fichero CSV descargado
  * Ir a tus listas y ahí veras las cuentas, las cuales puedes seguir manualmente, agregar a antenas...

## Consejos
* Seguir #etiquetas es una estupenda forma de descubrir gente y estar informado de muchos temas
* Rellenar vuestra bio lo más informativamente posible y crear un toot con la etiqueta #presentación contando vuestros intereses llenándola de #etiquetas que os descubra la gente con las mismas afinidades
* Ver [a quien sigue](https://masto.es/@DavidMarzalC/following) las personas que sigues (y así recursivamente)
* Usar la opción CW (Content Warning/Advertencia de contenido) al subir fotos de comida, spoilers...
* Los de [Vamonos juntas](https://vamonosjuntas.org/help)

## Para Geeks
* Probar el cliente web [Phanpy](https://phanpy.social/), es software libre y precioso no lo siguiente.
* Podéis silenciar palabras o etiquetas en la configuración de vuestra cuenta para esconder o no ver cierto contenido
* Estas listas se generan inicialmente (luego hay adiciones manuales) gracias a [este script](https://gitlab.com/Marzal/dmc-scripts/-/blob/master/bin/dmc_mastosplit.sh)