---
date: 2020-06-30
title: "Hola MundoLibre"
linkTitle: "Presentacion del blog"
description: "Breve descripción de lo que pretende ser y albergar este blog"
author: David Marzal ([@DavidMarzalC](https://masto.es/@DavidMarzalC)
resources:
- src: "**.{png,jpg}"
  title: "Image #:counter"
  params:
    byline: "Photo: Riona MacNamara / CC-BY-CA"
---

## Idea principal

En este blog pretendo dejar toda la documentación sobre los diferentes proyectos que me voy encontrando por si en el futuro le puede servir a alguien o a mi maltrecha memoria.
