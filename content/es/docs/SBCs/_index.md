---
title: "Placas SBC"
icon: fa-solid fa-microchip
linkTitle: "Placas SBC"
weight: 45
description: >
  Placas ARM: Raspberry Pi 2, Asus Tinker Board S
categories: [SBC]
---
