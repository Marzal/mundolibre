---
title: "DietPi Raspbian alternative"
icon: fa-brands fa-raspberry-pi
linkTitle: "DietPi"
weight: 1
date: 2020-09-15
description: >
    Alternativa ligera a Raspberry OS
categories: [SBC]
tags: [DIY,IOT]
---
* [Web oficial](https://dietpi.com) - [Guia de instalción](https://dietpi.com/docs/user-guide_install) - [Github](https://github.com/MichaIng/DietPi)
    * [Guia de instalación](https://dietpi.com/phpbb/viewtopic.php?p=9#p9) antigua pero más completa

## Preparación
* https://github.com/MichaIng/DietPi/blob/master/dietpi.txt
```bash
NET_HOSTNAME='ResiduoCero'
GLOBAL_PASSWORD='PasswordGlobal'
MySSID="'RedWiFi'"
MyWifiKey="'Password de la WiFi'"

sed -i 's/^AUTO_SETUP_ACCEPT_LICENSE=.*/AUTO_SETUP_ACCEPT_LICENSE=1/' dietpi.txt
sed -i 's/^AUTO_SETUP_LOCALE=.*/AUTO_SETUP_LOCALE=es_ES.UTF-8/' dietpi.txt
sed -i 's/^AUTO_SETUP_KEYBOARD_LAYOUT=.*/AUTO_SETUP_KEYBOARD_LAYOUT=es/' dietpi.txt
sed -i 's#^AUTO_SETUP_TIMEZONE=.*#AUTO_SETUP_TIMEZONE=Europe/Madrid#' dietpi.txt
sed -i 's/^AUTO_SETUP_NET_WIFI_ENABLED=.*/AUTO_SETUP_NET_WIFI_ENABLED=1/' dietpi.txt
sed -i 's/^AUTO_SETUP_NET_WIFI_COUNTRY_CODE=.*/AUTO_SETUP_NET_WIFI_COUNTRY_CODE=ES/' dietpi.txt
sed -i "s/^AUTO_SETUP_NET_HOSTNAME=.*/AUTO_SETUP_NET_HOSTNAME=$NET_HOSTNAME/" dietpi.txt
sed -i "s/^AUTO_SETUP_GLOBAL_PASSWORD=.*/AUTO_SETUP_GLOBAL_PASSWORD=$GLOBAL_PASSWORD/" dietpi.txt
sed -i 's/^AUTO_SETUP_HEADLESS=.*/AUTO_SETUP_HEADLESS=1/' dietpi.txt
sed -i 's/^SURVEY_OPTED_IN=.*/SURVEY_OPTED_IN=1/' dietpi.txt
sed -i 's/^CONFIG_SERIAL_CONSOLE_ENABLE=.*/CONFIG_SERIAL_CONSOLE_ENABLE=0/' dietpi.txt
echo "AUTO_SETUP_INSTALL_SOFTWARE_ID=20" >> dietpi.txt

sed -i "s/^aWIFI_SSID\[0\]=.*/aWIFI_SSID[0]=$MySSID/" dietpi-wifi.txt
sed -i "s/^aWIFI_KEY\[0\]=.*/aWIFI_KEY[0]=$MyWifiKey/" dietpi-wifi.txt
```

* Buscar IP y conectar
```bash
nmap -sn 192.168.1.0/24
ssh root@IP
```

* Recuperar HDMI si estás en HEADALESS
```bash
sed -i 's/^AUTO_SETUP_HEADLESS=.*/AUTO_SETUP_HEADLESS=0/' dietpi.txt
sed -i /^[[:blank:]]*max_framebuffers=/c\#max_framebuffers=2 config.txt
sed -i /^[[:blank:]]*hdmi_ignore_hotplug=/c\#hdmi_ignore_hotplug=0 config.txt
sed -i /^[[:blank:]]*enable_tvout=/c\#enable_tvout=0 config.txt

```

## Utilidades

* dietpi-launcher # ncurses
* dietpi-software # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-software)
* dietpi-config # [ncurses](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-config)
* dietpi-drive_manager # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-drive_manager)
* dietpi-autostart # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/)
* dietpi-services # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-services)
* dietpi-led_control [ncurses](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-led_control)
* dietpi-cron # [ncurses](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-cron)
* dietpi-update # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-update)
* dietpi-backup # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-backup)
* dietpi-sync [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-sync)
* dietpi-cleaner # [ncurses + cli](https://github.com/MichaIng/DietPi/blob/master/dietpi/dietpi-cleaner)

### Configuración SO
```bash
/boot/dietpi/func/dietpi-set_hardware headless 1
/boot/dietpi/func/dietpi-set_hardware gpumemsplit 16
/boot/dietpi/func/dietpi-set_hardware wificountrycode ES # OJO con los canales
/boot/dietpi/func/dietpi-set_hardware serialconsole disable
/boot/dietpi/func/dietpi-set_hardware i2c disable
/boot/dietpi/func/dietpi-set_hardware rpi-opengl disable
/boot/dietpi/func/dietpi-set_hardware rpi-camera enable
systemctl enable --now dietpi-wifi-monitor

/boot/dietpi/func/dietpi-set_swapfile 1 /mnt/usb0/swap

dietpi-software list
dietpi-software install 20
dietpi-software install 172
dietpi-autostart 0 # Manual
dietpi-services status
dietpi-backup 1 # Hacer backup
dietpi-sync 1
dietpi-cleaner 1

apt install -y openssh-client # dropbear lo necesita para poder recibir ficheros por scp
```

```bash
'disable_camera_led=' 'disable_camera_led=0' /boot/config.txt
```

## WiFi ##

### RPi 3A+ (bullseye 64) ###
* Hay varios canales desactivados
```bash
dmesg -H| grep -i fmac
 brcmf_c_preinit_dcmds: Firmware: BCM4345/6 wl0: Nov  1 2021 00:37:25 version 7.45.241 (1a2f2fa CY) FWID 01-703fd60
```
```bash
iw list | grep -P "5\d+ MHz"
                        * 2452 MHz [9] (20.0 dBm)
                        * 2457 MHz [10] (20.0 dBm)
                        * 5170 MHz [34] (disabled)
                        * 5180 MHz [36] (20.0 dBm)
                        * 5190 MHz [38] (disabled)
                        * 5200 MHz [40] (20.0 dBm)
                        * 5210 MHz [42] (disabled)
                        * 5220 MHz [44] (20.0 dBm)
                        * 5230 MHz [46] (disabled)
                        * 5240 MHz [48] (20.0 dBm)
                        * 5260 MHz [52] (20.0 dBm) (no IR, radar detection)
                        * 5280 MHz [56] (20.0 dBm) (no IR, radar detection)
                        * 5300 MHz [60] (20.0 dBm) (no IR, radar detection)
                        * 5320 MHz [64] (20.0 dBm) (no IR, radar detection)
                        * 5500 MHz [100] (20.0 dBm) (no IR, radar detection)
                        * 5520 MHz [104] (20.0 dBm) (no IR, radar detection)
                        * 5540 MHz [108] (20.0 dBm) (no IR, radar detection)
                        * 5560 MHz [112] (20.0 dBm) (no IR, radar detection)
                        * 5580 MHz [116] (20.0 dBm) (no IR, radar detection)
                        * 5600 MHz [120] (20.0 dBm) (no IR, radar detection)
                        * 5620 MHz [124] (20.0 dBm) (no IR, radar detection)
                        * 5640 MHz [128] (20.0 dBm) (no IR, radar detection)
                        * 5660 MHz [132] (20.0 dBm) (no IR, radar detection)
                        * 5680 MHz [136] (20.0 dBm) (no IR, radar detection)
                        * 5700 MHz [140] (20.0 dBm) (no IR, radar detection)
                        * 5720 MHz [144] (disabled)
                        * 5745 MHz [149] (disabled)
                        * 5765 MHz [153] (disabled)
                        * 5785 MHz [157] (disabled)
                        * 5805 MHz [161] (disabled)
                        * 5825 MHz [165] (disabled)
```
```bash
iw reg get
global
country ES: DFS-ETSI
        (2400 - 2483 @ 40), (N/A, 20), (N/A)
        (5150 - 5250 @ 80), (N/A, 23), (N/A), NO-OUTDOOR, AUTO-BW
        (5250 - 5350 @ 80), (N/A, 20), (0 ms), NO-OUTDOOR, DFS, AUTO-BW
        (5470 - 5725 @ 160), (N/A, 26), (0 ms), DFS
        (5725 - 5875 @ 80), (N/A, 13), (N/A)
        (57000 - 66000 @ 2160), (N/A, 40), (N/A)

phy#0
country 99: DFS-UNSET
        (2402 - 2482 @ 40), (6, 20), (N/A)
        (2474 - 2494 @ 20), (6, 20), (N/A)
        (5140 - 5360 @ 160), (6, 20), (N/A)
        (5460 - 5860 @ 160), (6, 20), (N/A)
```