---
title: "Gestor de servicios"
icon: fa-solid fa-d
linkTitle: "systemD"
weight: 10
date: 2020-12-15
description: >
    Comandos utiles
categories: [Sistemas]
tags: [sysadmin]
---

* Mostrar gestores de energia
```bash
systemd-inhibit --list
```
* Mostrar información sin acortar
```bash
systemctl --no-pager -l status sshd.service
systemctl --no-pager --full status sshd.service
```

* Listar servicios
```bash
systemctl list-unit-files

systemctl list-units --state=failed
```

* Mostrar solo mensajes de error del encendido actual
```bash
journalctl -b -p 3
journalctl -b --priority=err

```
## Orden de arranque
```bash
systemd-analyze blame

systemctl get-default   # Get running target
systemctl list-units --type target 
```

## Dependencias
* https://unix.stackexchange.com/questions/213185/restarting-systemd-service-on-dependency-failure

## Documentación
* https://wiki.archlinux.org/index.php/Systemd
