---
title: "Seguridad y hardening"
icon: fa-solid fa-shield-halved
linkTitle: "Seguridad"
weight: 4
date: 2023-03-09
description: >
  Seguridad, hardening... sobre todo para ArchLinux
categories: [Sistemas]
tags: [hardening,privacidad]
---

* https://wiki.archlinux.org/index.php/Security

## linux-hardened
[Es](https://www.archlinux.org/packages/extra/x86_64/linux-hardened/) el mismo que el normal pero con unas [modificaciones](https://github.com/anthraxx/linux-hardened/releases) para configurarlo más seguro.

* https://wiki.archlinux.org/index.php/Security#Kernel_hardening

### Efectos secundarios
* debugfs
  * Tener __CONFIG_DEBUG_FS_ALLOW_NONE=y__ en vez de __CONFIG_DEBUG_FS_ALLOW_ALL__ hace que no exista, ni se pueda usar, `/sys/kernel/debug`
  ```bash
  kernel: pinctrl core: failed to create debugfs directory
  kernel: zswap: debugfs initialization failed
  systemd[1]: Condition check resulted in Kernel Debug File System being skipped.
  kernel: cec: Failed to create debugfs cec dir
  kernel: [drm:amdgpu_debugfs_init [amdgpu]] *ERROR* Failed to init debugfs
  kernel: [drm:amdgpu_pci_probe [amdgpu]] *ERROR* Creating debugfs files failed (-1).
  ```

## Audit Framwork
[Audit](/mundolibre/docs/sistemas/hardening#AUDIT)

## Mandatory Access Control
https://wiki.archlinux.org/index.php/Security#Mandatory_access_control
### Apparmor
* https://wiki.archlinux.org/index.php/AppArmor

* Comprobaciones:
```bash
grep lsm= /etc/default/grub # ¿Activado en el grub?
# Debe devolver esto
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet lsm=lockdown,yama,apparmor"
```

```bash
zgrep -i APPARMOR /proc/config.gz   # Ver si el kernel lo tiene configurado
# Resultado con linux-hardened
CONFIG_SECURITY_APPARMOR=y
CONFIG_SECURITY_APPARMOR_HASH=y
CONFIG_SECURITY_APPARMOR_HASH_DEFAULT=y
# CONFIG_SECURITY_APPARMOR_DEBUG is not set
# CONFIG_DEFAULT_SECURITY_APPARMOR is not set
```

```bash
zgrep CONFIG_LSM= /proc/config.gz   # kernel's configured lsm
# Este es el por defecto
CONFIG_LSM="lockdown,yama"

# El valor actual se puede comprobar con:
cat /sys/kernel/security/lsm
# Al cual se le añade siempre capability automaticamente
capability,lockdown,yama
```

```bash
systemctl --version   # Ver si systemD está compilado con soporte de apparmor
# La versión 246 a 2020-11-22 NO 
systemd 246 (246.6-1-arch)
+PAM +AUDIT -SELINUX -IMA -APPARMOR +SMACK -SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +ZSTD +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=hybrid
# https://bugs.archlinux.org/task/62560
```

* Configuración
  * Añadir `apparmor=1 lsm=lockdown,yama,apparmor` a la linea __GRUB_CMDLINE_LINUX_DEFAULT__ en `/etc/default/grub`
    * Reconstruir el grub con `grub-mkconfig -o /boot/grub/grub.cfg`
  * `systemctl enable apparmor.service`

* Uso
```bash
aa-enabled -x || aa-enabled       # Comprobar si está habilitado
aa-status || cat /sys/kernel/security/apparmor/profiles  # Display the current loaded status
ps auxZ | grep -v '^unconfined'   # Mostrar programas ejecución confinados
aa-unconfined                     # Mostrar procesos con puertos en escucha sin profiles asociados
aa-genprof /path/to/application   # Generar profiles
aa-logprof                        # Revisar logs y sugerir cambios en profiles
ausearch -m avc                   # Busca logs en audit pero no funciona
aa-complain $PROFILE              # Poner profile en modo debug
aa-enforce $PROFILE               # Poner profile en modo activo
systemctl reload apparmor.service # Recargar todos los profiles
```
* Los profiles activados están en `/etc/apparmor.d`
  * Por defecto el paquete solo pone los que están maduros
  * El resto están en `/usr/share/apparmor/extra-profiles/`
* Extras
  * Activar un profile de extra : `ln -s /usr/share/apparmor/extra-profiles/usr.bin.apropos /etc/apparmor.d/`
  * No van:
    * usr.lib.firefox.firefox
  * Funcionan:
    * `ln -s /usr/share/apparmor/extra-profiles/usr.bin.apropos /etc/apparmor.d/`
    * `ln -s /usr/share/apparmor/extra-profiles/usr.bin.man /etc/apparmor.d/`
    * `ln -s /usr/share/apparmor/extra-profiles/usr.bin.passwd /etc/apparmor.d/`
    * `ln -s /usr/share/apparmor/extra-profiles/usr.sbin.sshd /etc/apparmor.d/`
    * `ln -s /usr/share/apparmor/extra-profiles/usr.sbin.useradd /etc/apparmor.d/`
    * `ln -s /usr/share/apparmor/extra-profiles/usr.sbin.userdel /etc/apparmor.d/`
    * `ln -s /usr/share/apparmor/extra-profiles/usr.bin.chromium-browser /etc/apparmor.d/` Pero no el ADB con Android

* Docu
  * https://wiki.debian.org/AppArmor/
  * https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/AppArmorProfiles

### SELinux

## Security packaging guidelines
* https://wiki.archlinux.org/index.php/Arch_package_guidelines/Security
```bash
checksec --file=/usr/bin/cat
```

## AUDIT
* Info
```bash
auditctl -s # reports the status of the Audit syste
systemctl status systemd-journald-audit.socket
systemctl status auditd.service
auditctl -l # lists all currently loaded Audit rules
```

* Config
```bash
auditctl -D # deletes all currently loaded Audit rules
augenrules --load
```

* Reportes
```bash
aureport -n # Look for abnormalities
ausearch
```

### Configuración
#### Control Rules
```bash
-D      # Delete all previous rules
-b 8192 # Set buffer size
-e 2    # Make the configuration immutable -- reboot is required to change audit rules
-f 2    # Panic when a failure occurs
-r 100  # Generate at most 100 audit messages per second
--loginuid-immutable 1  # Make login UID immutable once it is set (may break containers)
```

### Documentación
* https://wiki.archlinux.org/index.php/Audit_framework
* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-defining_audit_rules_and_controls
