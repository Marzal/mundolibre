## Bare repo

git init --bare ${HOME}/.config.git

* https://superuser.com/questions/1504543/commit-symlink-outside-of-repo-to-git

```bash
# Init repo
git init --bare ${HOME}/gitlab/scripts.git

# Ignore all files except the ones you want to track
config config --local status.showUntrackedFiles no

# Add an alias to your .bashrc for convenience
echo "alias config='/usr/bin/git --git-dir=${HOME}/.config.git/ --work-tree=${HOME}'" >> ${HOME}/.bashrc

# Start using your repo to track your dotfiles
config add ~/.bashrc ~/.vimrc ...
```