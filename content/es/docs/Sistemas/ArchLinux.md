---
title: "Distribución ArchLinux"
icon: fa-brands fa-linux
linkTitle: "ArchLinux"
weight: 2
date: 2021-02-07
description: >
  Configuración, rendimiento...
categories: [Sistemas]
tags: [sysadmin]
---

## Guias
* https://itsfoss.com/install-arch-linux/
* https://denovatoanovato.net/instalar-arch-linux/

## Previo
* <https://wiki.archlinux.org/title/Install_Arch_Linux_via_SSH>
```bash
passwd
grep 'PermitRootLogin yes' /etc/ssh/sshd_config
```

## Instalación manual

* <https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment>

```bash
screen # tmux
# https://wiki.archlinux.org/title/Installation_guide#Connect_to_the_internet
ip link
ip a 
ping -c 4 archlinux.org
# resolvectl status
# wifi-menu
# Si hay red podemos seguir desde el otro pc con ssh

localectl list-keymaps | grep es
find /usr/share/kbd/keymaps/ -type f -name "*es*.map.gz"
#basename -s .map.gz /usr/share/kbd/keymaps/**/*.map.gz | grep es
loadkeys es
#setxkbmap -layout es
#localectl set-x11-keymap es

ls /sys/firmware/efi/efivars

# timedatectl set-ntp true
timedatectl

fdisk -l
fdisk /dev/nvme0n1
#cfdisk
# 512M para efi
lsblk 

# Si queremos encriptación
# cryptsetup luksFormat /dev/nvme0n1p2
# cryptsetup luksOpen /dev/nvme0n1p2 system
# mkfs.ext4 /dev/mapper/system

mkfs.ext4 /dev/nvme0n1p2

# EFI si la hemos creado NOSOTROS
mkfs.fat -F32 /dev/nvme0n1p1

mount /dev/nvme0n1p2 /mnt
mount --mkdir /dev/nvme0n1p1 /mnt/efi

pacstrap -K /mnt base linux linux-firmware
# -i
genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
hwclock --systohc
# --utc
date

sed -i '/#es_ES.UTF-8/s/^#//' /etc/locale.gen
sed -i '/#en_US.UTF-8/s/^#//' /etc/locale.gen
locale-gen
echo 'LANG=es_ES.UTF-8' > /etc/locale.conf
echo 'KEYMAP=es' > /etc/vconsole.conf
echo 'residuocero' > /etc/hostname
echo '127.0.1.1	residuocero.localdomain	residuocero' >> /etc/hosts

#mkinitcpio -P

passwd

# https://wiki.archlinux.org/title/Microcode
# https://wiki.archlinux.org/title/Network_configuration#Network_management
# https://wiki.archlinux.org/title/NetworkManager
pacman -S grub efibootmgr bash-completion networkmanager openssh intel-ucode #os-prober

## GRUB carga automáticamente el firmware 
# https://wiki.archlinux.org/title/Arch_boot_process#Boot_loader
# https://wiki.archlinux.org/title/GRUB

#grub-install --target=i386-pc /dev/sdX # BIOS/MBR
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --recheck --removable
#os-prober
grub-mkconfig -o /boot/grub/grub.cfg


systemctl enable NetworkManager
systemctl enable sshd

PRIMARYUSER=residuocero
useradd -m -G adm $PRIMARYUSER
passwd $PRIMARYUSER

exit
umount -R /mnt ; reboot


# https://wiki.archlinux.org/title/General_recommendations

localectl status
```

## Primeros pasos
### Basicos
```bash
pacman -S sudo pkgfile vim
sed -i '/wheel ALL=(ALL) ALL/s/^#//' /etc/sudoers
```

### Network Manager
```bash
systemctl --type=service | grep -i net
systemctl status sshd
systemctl status dhcpcd
systemctl status NetworkManager
# pacman -S networkmanager
```

### SWAP
* No la uso pero hay algo en script dmc_archlinux.sh al respecto.

### Mantenimiento
  * <https://wiki.archlinux.org/index.php/System_maintenance>
```bash
systemctl --failed
journalctl -p 3 -xb
# https://wiki.archlinux.org/title/Solid_state_drive#TRIM
lsblk --discard
hdparm -I /dev/sda | grep TRIM
systemctl enable fstrim.timer --now
systemctl status fstrim.timer

# https://wiki.archlinux.org/index.php/Improving_performance
```

## Entornos gráficos

### KDE Plasma
* <https://community.kde.org/Distributions/Packaging_Recommendations>
* <https://userbase.kde.org/Phonon>
```bash
pacman -S plasma-meta plasma-wayland-session #plasma-desktop
pacman -S okular kate konsole dolphin kompare ktorrent

#kdeplasma-addons plasma-nm kfind
systemctl status sddm
systemctl enable sddm
#pacman -S spice-vdagent
```

### Cualquiera
```bash
pacman -S fltapak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

pacman -S bluez
pacman -S firefox firefox-i18n-es-es ark libreoffice hunspell hunspell-es_es
pacman -S vlc libreoffice-fresh-es

```