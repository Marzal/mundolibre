```json
{
    "telemetry.telemetryLevel": "off",
    "workbench.iconTheme": "vscode-icons",
    "workbench.startupEditor": "newUntitledFile",
    "workbench.enableExperiments": false,
    "shellcheck.customArgs": [
        "-x"
    ],
    // Priority: user defined > bundled shellcheck binary > "shellcheck"
    "shellcheck.executablePath": "/usr/bin/shellcheck",
    "gitlens.views.repositories.branches.layout": "list",
    "gitlens.views.repositories.files.layout": "list",
    "gitlens.codeLens.scopes": [
        "document"
    ],
    "git.autofetch": true,
    "git.confirmSync": false,
    "editor.renderControlCharacters": true,
    "editor.rulers": [
        {
            "column": 0,
            "color": "#5a5a5a80"
        }, // left boundary is 50% opaque
        {
            "column": 2,
            "color": "#5a5a5a20"
        }, // tab stops are 12.5% opaque
        {
            "column": 4,
            "color": "#5a5a5a20"
        },
        {
            "column": 6,
            "color": "#5a5a5a20"
        },
        {
            "column": 8,
            "color": "#5a5a5a20"
        },
        {
            "column": 10,
            "color": "#5a5a5a20"
        },
        {
            "column": 40,
            "color": "#5a5a5a20"
        }, // center line
        {
            "column": 79,
            "color": "#5a5a5a20"
        }, // right rule minus one
        {
            "column": 80,
            "color": "#5a5a5a80"
        }, // right rule
        {
            "column": 120,
            "color": "#5a5a5a40"
        } // extra right rule
    ],

    "editor.bracketPairColorization.enabled": true,
    "diffEditor.ignoreTrimWhitespace": false,

    // Blockman - Highlight Nested Code Blocks
    "editor.inlayHints.enabled": "off",
    "editor.guides.indentation": false, // new API for indent guides. The old one is: "editor.renderIndentGuides": false,
    "editor.guides.bracketPairs": false, // advanced indent guides (But only for brackets) (This does not turn off editor.bracketPairColorization)
    "editor.wordWrap": "off", // Blocks will not render properly if there is any word wrapping
    "diffEditor.wordWrap": "off",
    "workbench.colorCustomizations": {
        "editor.lineHighlightBorder": "#9fced11f",
        "editor.lineHighlightBackground": "#1073cf2d"
    },// FIN: Blockman - Highlight Nested Code Blocks

    "evenBetterToml.formatter.allowedBlankLines": 2,
    "linkcheckerhtml.reportBadChars": "Don't report",
    "linkcheckerhtml.reportHTTPSAvailable": "as Warning",
    "linkcheckerhtml.reportSemanticErrors": "as Warning",
    "editor.stickyScroll.enabled": true,
    "workbench.colorTheme": "Monokai"

}
```