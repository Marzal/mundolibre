---
title: "Instalación"
icon: fa-solid fa-ferry
linkTitle: "Instalación"
weight: 1
date: 2024-11-10
description: >
  Instalación en GNU/Linux
categories: [Contenedores]
tags: [docker]
---
## ArchLinux

```bash
pacman -S docker-compose docker
#O a mano : https://docs.docker.com/compose/install/linux/
```

## debian

```Bash
sudo apt-get remove docker docker-engine docker.io containerd runc
apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -
echo "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list

apt update
apt install docker-ce
```
