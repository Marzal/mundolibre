---
title: "Administración"
icon: fa-solid fa-anchor
linkTitle: "Administración"
weight: 2
date: 2020-09-26
description: >
  Administración de Docker
categories: [Contenedores]
tags: [docker]
---

## Info

```Bash
docker version
docker image ls
docker volume ls
docker network ls
docker container ls | docker ps
docker info

```

## Update image & container
```bash
docker images
docker ps
docker stop $container_id
docker rm $container_id
docker pull portainer/portainer-ce
docker run ...
```
* [Portainer](https://docs.portainer.io/v/ce-2.11/start/upgrade/docker)

    docker-compose pull portainer && docker-compose up -d --force-recreate portainer

## Docker Compose

### No confundir .env con env_file
* https://docs.docker.com/compose/compose-file/compose-file-v3/#env_file
* https://rotempinchevskiboguslavsky.medium.com/environment-variables-in-container-vs-docker-compose-file-2426b2ec7d8b
* https://vsupalov.com/docker-arg-env-variable-guide/
* https://dimmaski.com/env-files-docker/
