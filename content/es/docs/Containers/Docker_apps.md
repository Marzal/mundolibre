---
title: "Aplicaciones"
icon: fa-solid fa-ship
linkTitle: "Aplicaciones"
weight: 3
date: 2021-02-22
description: >
  Servicios de DockerHub
categories: [Contenedores]
tags: [docker]
---

## Adminstradores de contenedores
### portainer

* Instalación 
  * <https://www.portainer.io/installation/>
  * <https://portainer.readthedocs.io/en/latest/deployment.html>
  * <https://documentation.portainer.io/v2.0/deploy/linux/>
```Bash
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9443:9443 \
    --name portainer --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce
```

### Diun
* [Web](https://crazymax.dev/diun/) - [DockerHub](https://hub.docker.com/r/crazymax/diun/)
* [Docker-compose](https://crazymax.dev/diun/install/docker/)

### Watchtower
* [Web](https://containrrr.dev/watchtower/) - [Github](https://github.com/containrrr/watchtower) - [DockerHub](https://registry.hub.docker.com/r/containrrr/watchtower)

```bash
docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock -v /etc/timezone:/etc/timezone:ro containrrr/watchtower --cleanup --include-stopped  --interval 3600 --monitor-only #--debug --run-once
```

### Ouroboros
- [Github](https://github.com/pyouroboros/ouroboros) - 
  * En mantenimiento comunitario/semi-abandonado (1.4.3 2019-12-11)
```bash
docker run -d --name ouroboros -v /var/run/docker.sock:/var/run/docker.sock pyouroboros/ouroboros
```

## Proxy Inverso
### Traefik
* Info
  * [Web](https://traefik.io/) - [GitHub](https://github.com/traefik/traefik)

* Contenedor
  * Con docker-compose.yml
  ```yml
  version: '3'

  services:
    reverse-proxy:
      # The official v2 Traefik docker image
      image: traefik:latest
      container_name: traefik
      # Enables the web UI and tells Traefik to listen to docker
      command: --api.insecure=true --providers.docker
      ports:
        # The HTTP port
        - "80:80"
        # The Web UI (enabled by --api.insecure=true)
        - "8080:8080"
      volumes:
        # So that Traefik can listen to the Docker events
        - /var/run/docker.sock:/var/run/docker.sock
        # Atareao
        - ./traefik.yml:/etc/traefik/traefik.yml
  ```
  * Por comando
  ```bash
  wget https://raw.githubusercontent.com/traefik/traefik/master/traefik.sample.toml -O traefik.toml
  docker run -d -p 8080:8080 -p 80:80 \
    -v $PWD/traefik.yml:/etc/traefik/traefik.yml traefik:latest
  ```
  * Configuración
    * traefik.yml
    ```yml
    entryPoints:
    web:
      address: :80
    websecure:
      address: :443
    api:
      insecure: true
    providers:
      docker:
        endpoint: "unix:///var/run/docker.sock"
    ```

* Probarlo : `curl localhost:8080/api/rawdata`
  * docker-compose.yml
    ```yml
    whoami:
    image: traefik/whoami
    labels:
      - "traefik.http.routers.whoami.rule=Host(`whoami.localhost`)"
    ```
  * `curl -H Host:whoami.localhost http://localhost`
* Para usarlo
```yml
services:
  SERVICIO:
    networks:
      - proxy
    labels:
      - traefik.enable=true
      - traefik.http.routers.SERVICIO.entrypoints=http
      - traefik.http.routers.SERVICIO.rule=Host(`SERVICIO.tu-servidor.com`)
      - traefik.http.middlewares.SERVICIO-https-redirect.redirectscheme.scheme=https
      - traefik.http.routers.SERVICIO.middlewares=SERVICIO-https-redirect
      - traefik.http.routers.SERVICIO-secure.entrypoints=https
      - traefik.http.routers.SERVICIO-secure.rule=Host(`SERVICIO.tu-servidor.com`)
      - traefik.http.routers.SERVICIO-secure.tls=true
      - traefik.http.routers.SERVICIO-secure.tls.certresolver=myresolver

networks:
  proxy:
    external: true
```
  * Ejemplos -> <https://github.com/DoTheEvo/Traefik-v2-examples>

* Tutoriales
  * [Geekland](https://geekland.eu/instalar-y-configurar-traefik-v2-para-usarlo-como-proxy-inverso)
### Ngnix securizado
 * https://blog.segu-info.com.ar/2021/01/imagen-de-nginx-docker-segura-de-forma.html

### Caddy
* https://ugeek.github.io/blog/post/2021-02-19-proxy-inverso-con-caddy.html

### DDNS
```
Servicio - Pais - subdomains - realtime - 

dyndns - - - .org
TZO - http://www.tzo.com/ (error)
NoIP - US - 3 -  - Verificar cada 30 días - https://www.noip.com/free
FreeDNS US/CA- 5 - https://freedns.afraid.org
  * Públicas: 10x.es / moot.es / galitec.es / open.ga / garantialibre.com / freesoftwares.ir
  * Privadas: foroupct.es / soft-libre.es
DNSDynamic - www.dnsdynamic.org (error)

dyndnss - Germany - 3 - SI - 
```

## Office
### etherpad

<https://github.com/ether/etherpad-lite/blob/develop/doc/docker.md>

```Bash
docker run -d --name=etherpad -p 9001:9001 -v etherpad_data:/opt/etherpad-lite/var -e EP_TITLE="GNU/Linux Valencia" -e "ADMIN_PASSWORD=XXXX" -e "PAD_OPTIONS_LANG=es-es" -e "DB_TYPE=SQLite" -e 'DEFAULT_PAD_TEXT=Bienvenido a Etherpad! Este servicio es ofrecido por GNU/Linux Valencia' etherpad/etherpad
```

#### jitsi/etherpad
* [DockerHub](https://hub.docker.com/r/jitsi/etherpad) - [GitHub](ttps://github.com/jitsi/jitsi-meet/wiki/Etherpad-addition-example) - [GitHub Wiki](https://github.com/jitsi/docker-jitsi-meet)

```bash
docker-compose -f docker-compose.yml -f etherpad.yml up
```
## Streaming
### Owncast
* [DockerHub](https://hub.docker.com/r/gabekangas/owncast)
* Info
  * [Configuración](/mundolibre/docs/multimedia/streaming/#owncast) - [Tutorial en vídeo](https://devtube.dev-wiki.de/videos/watch/8f26c4b6-60cb-45af-8c39-1d7bf50d5d7d)
* [Ejecución rápida](https://owncast.online/quickstart/docker/) : `docker run -v "$(pwd)/data:/app/data" -p 8080:8080 -p 1935:1935 -it gabekangas/owncast:latest`
#### docker build
<https://owncast.online/docs/building/>
* Crear un contenedor desde las fuentes de git
```bash
git clone https://github.com/owncast/owncast
cd owncast
cat Dockerfile    # Ver lo que va a realizar el build
docker build -t owncast-build-git .   # Etiquetamos y lanzamos sobre carpeta actual
docker run -v "$(pwd)/data:/app/data" -p 8080:8080 -p 1935:1935 -it --name owncast_build_git owncast-build-git
```
#### docker-compose
* Ejemplo que instala `gabekangas/owncast:latest` + `traefik:latest`
  * Funciona tal cual, pero puedes ajustar
  ```bash
  mkdir owncast && cd owncast
  #wget https://raw.githubusercontent.com/owncast/owncast/master/examples/docker-compose.yaml
  wget https://gitlab.com/Marzal/mundolibre/-/raw/master/content/es/docs/Containers/compose/owncast-traefik.yaml -O docker-compose.yaml
  sed -i s/live.your.org/tuDOMINIO/ docker-compose.yaml
  sed -i s/postmaster@mydomain.com/tuEMAIL/ docker-compose.yaml
  docker-compose up
  ```
#### Geo-IP
* /app/data/GeoLite2-City.mmd
* Se puede descargar de: https://github.com/P3TERX/GeoLite.mmdb

### BigBlueButton-liveStreaming
Permite emitir y grabar la sala.
* [Github](https://github.com/aau-zid/BigBlueButton-liveStreaming)
* [Apuntes sobre BBB](/mundolibre/docs/multimedia/bbb)

### Restreamer
* [Web](https://datarhei.github.io/restreamer/)
* Uso
  * Interfaz : <http://localhost:8083>
  * Conexion : `rtmp://127.0.0.1/live/obs.stream?token=$RS_TOKEN`

```bash
docker run -d --restart no \
     --name restreamer \
     -e "RS_USERNAME=cambiar" -e "RS_PASSWORD=esto" \
     -p 8083:8080 -v "$(pwd)/db:/restreamer/db" \
     -p 1935:1935 -e "RS_TOKEN=clavestream" \
     datarhei/restreamer:latest
```

### Mumble Server (Murmur)
* [Github](https://github.com/mumble-voip/mumble-docker) - [DockerHub](https://hub.docker.com/r/mumblevoip/mumble-server)
* Alternativas no oficiales:
  * https://hub.docker.com/r/hetsh/murmur + https://hub.docker.com/r/avhost/docker-murmur
* Compose
  * Ejemplos: [mumo + postgress](https://github.com/mumble-voip/mumble-docker/wiki/docker_example_mumble_mumo_postgres)
  * Oficial: (la propia imagen mapea el volumen /data)
```yml
version: "3"
services:
    mumble-server:
        image: mumblevoip/mumble-server
        container_name: mumble-server
        hostname: mumble-server
        restart: on-failure
        ports:
            - 64738:64738
            - 64738:64738/udp
        environment:
            MUMBLE_CONFIG_SERVER_PASSWORD: "QueSiNoEntraCualquiera"
            MUMBLE_CONFIG_USERS: "10"
            MUMBLE_CONFIG_BANDWIDTH: "128000"
            MUMBLE_CONFIG_IMAGEMESSAGELENGTH: 1310720
            MUMBLE_CONFIG_REGISTERNAME: Gnu Linux Valencia
            MUMBLE_CONFIG_USERNAME: '[ -=\\w\\[\\]\\{\\}\\(\\)\\@\\|\\.]+'
            MUMBLE_CONFIG_CHANNELNAME: '[ \\-=\\w\\#\\[\\]\\{\\}\\(\\)\\@\\|\\%\\'']+'
            MUMBLE_CONFIG_WELCOMETEXT: 'Buenas'
            MUMBLE_CONFIG_opusthreshold: "50"
            MUMBLE_VERBOSE: 1

```
## Peertube

* [Docu](https://docs.joinpeertube.org/install-docker)

```bash
curl https://raw.githubusercontent.com/chocobozzz/PeerTube/develop/support/docker/production/docker-compose.yml > docker-compose.yml
```

```bash
curl https://raw.githubusercontent.com/Chocobozzz/PeerTube/0a25749f14a083d2c388b6229cbdbba695d0387e/support/docker/production/Dockerfile.buster > Dockerfile
docker run -d --restart no --name peertube-tools -it chocobozzz/peertube:develop-buster
```

## Utilidades

### REUSE
* Info
  * [Web](https://reuse.software/) - [Github](https://github.com/fsfe/reuse-tool)
```bash
docker run --rm --volume $(pwd):/data fsfe/reuse # Muestra si cumple
docker run --rm --volume $(pwd):/data fsfe/reuse download --all # Descarga las licencias definidas en el proyecto
docker run --rm --volume $(pwd):/data fsfe/reuse --include-submodules spdx -o out.spdx # Genera un informe
```

### ssh-audit
```bash
docker run --rm -it -p 2222:2222 positronsecurity/ssh-audit {ssh-server-ip}
```

### mquery
Para saber que plataformas soporta una imagen
```bash
docker run --rm mplatform/mquery anonaddy/anonaddy:latest
```


## Por investigar
### Mycroft
* Info
  * <https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/get-mycroft/docker>
  ```bash
  docker run -d  
    -v directory_on_local_machine:/root/.mycroft  
    --device /dev/snd  
    -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native  
    -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native  
    -v ~/.config/pulse/cookie:/root/.config/pulse/cookie  
    -p 8181:8181  
    --name mycroft mycroftai/docker-mycroft
  ```

### sysdig
* Monitorizar y debug del sistema
  * [Web](https://github.com/draios/sysdig) - [DockerHub](https://hub.docker.com/r/sysdig/sysdig)
* Requiere tener instalados los headers primero según la [docu](https://github.com/draios/sysdig/wiki/How-to-Install-Sysdig-for-Linux)

```bash
docker run -i -t --name sysdig --privileged -v /var/run/docker.sock:/host/var/run/docker.sock -v /dev:/host/dev -v /proc:/host/proc:ro -v /boot:/host/boot:ro -v /lib/modules:/host/lib/modules:ro -v /usr:/host/usr:ro sysdig/sysdig
```