---
title: "Veganismo y SL"
icon: fa-solid fa-feather-pointed
linkTitle: "Veganismo"
weight: 3
date: 2021-05-09
description: >
  Recursos y aplicaciones
categories: [Sostenibilidad]
tags: [Sostenibilidad,vegan]
---

## OpenStreetMaps

### Mapas web
https://veggiekarte.de/?lang=en
https://openvegemap.netlib.re/

### Etiquetas
* https://wiki.openstreetmap.org/wiki/Key:diet
  * [diet:vegetarian](https://taginfo.openstreetmap.org/keys/?key=diet%3Avegetarian#values)
  * [diet:vegan](https://taginfo.openstreetmap.org/keys/diet:vegan#values)

* https://wiki.openstreetmap.org/wiki/Tag:amenity%3Danimal_shelter

#### Overpass-turbo
https://overpass-turbo.eu/
* Detectar errores de etiquetado
```xml
nwr
  ["cuisine"~".vegan."]
  ({{bbox}});
out;

nwr["cuisine"~"[vV]eg(etari)?an"]({{bbox}});
out;
```

### Documentación
* Guia para colaborar - https://etherpad.snopyta.org/p/r.e721086d57cbd6a4c5c59c7a3f554fc3
* Crear marcas - https://help.openstreetmap.org/questions/69377/creating-brand-in-osm

### Utilidades
* <https://umap.openstreetmap.fr/en/> - Creación de mapas con capas de OpenStreetMaps
* https://f-droid.org/packages/app.fedilab.openmaps/

## Wikidata
* https://www.wikidata.org/wiki/Wikidata:Property_proposal/Barnivore_product_ID

## Reviews
https://lib.reviews/team/veg(etari)an

## Charlas
* [Software libre, veganismo y sostenibilidad](https://devtube.dev-wiki.de/videos/watch/55ed35ce-b270-4065-9e9c-a8da4ade6837)

## Proyectos
* https://veganhacktivists.org/