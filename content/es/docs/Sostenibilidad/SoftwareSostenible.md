---
title: "Sostenibilidad y Residuo Cero"
icon: fa-brands fa-osi
linkTitle: "Conocimiento abierto"
weight: 2
date: 2021-07-01
description: |
  Como puede el SL y el conocimiento abierto apoyar a la sostenibilidad y porqué es Zero Waste
categories: [Sostenibilidad]
tags: [Sostenibilidad]
---

## Contenido divulgativo

### Reutilizar
* [PodcastLinux 130 - Reutilizar portatiles ThinkPad](https://podcastlinux.com/posts/podcastlinux/130-Podcast-Linux/)
* [PodcastLinux 32 - Linux Connexion con Reciclanet](https://podcastlinux.com/posts/podcastlinux/32-Podcast-Linux/)

### Artículos

* https://blog.redigit.es/software-libre-y-medio-ambiente-una-relacion-beneficiosa-para-todos/
* Material informático y contaminación medioambiental - [Youtube](https://www.youtube.com/watch?v=OeMnMvWLpKo&t)
* Ejemplos aplicaciones
    * [Linux Adictos; Medioambiente: Open-source contra el cambio climático](https://www.linuxadictos.com/open-source-luchar-contra-cambio-climatico.html)
    * [Open Source y el cambio climatico.8 proyectos amigables con el medio ambiente](https://openexpoeurope.com/es/open-source-y-el-cambio-climatico-8-proyectos-de-codigo-abierto-amigables-con-el-medio-ambiente-que-debes-conocer/)
* Casos de uso
    * [Cómo hacer un Estudio de Impacto Ambiental (EIA) con solo Software Libre?](https://gidahatari.com/ih-es/como-hacer-un-estudio-de-impacto-ambiental-eia-con-solo-software-libre)
    * [La Agencia Ambiental Irlandesa ganadora del «Premio eGov de Código abierto»](https://www.mancomun.gal/es/noticias/la-agencia-ambiental-irlandesa-ganadora-del-premio-egov-de-codigo-abierto/)
* Argumentos
    * [Centro de Desarrollo de Competencias Digitales de Castilla-La Mancha](https://www.bilib.es/actualidad/blog/noticia/articulo/el-software-libre-es-ecologico/)
    * https://blogs.20minutos.es/codigo-abierto/2012/09/11/ecologia-de-codigo-abierto/
    * https://plasticfreeswindon.org/take_action/
    * https://zerowastemillennial.wordpress.com/2015/06/03/a-zero-waste-case-against-proprietary-software-and-surveillance/
* Otros idiomas
    * https://opensource.com/article/22/5/preserve-earths-livability-open-source


## Iniciativas

### Call for Code
* Promotores
    * https://linuxfoundation.org/projects/call-for-code/
    * https://developer.ibm.com/callforcode/
* Artículos
    * https://blog.desdelinux.net/call-for-code-iniciativa-ti-mundial-progreso-desarrollo-sostenible

### All For Climate
Host de Open Collective dedicado a movimientos por el clima y la justicia social
* https://opencollective.com/allforclimate

### Reuse electronics ensuring final recycling
* https://www.ereuse.org/

### Union Europea
* https://joinup.ec.europa.eu/collection/environment

### Green Software Foundation
https://greensoftware.foundation/

* Artículos
    * https://www.linuxadictos.com/green-software-foundation-una-fundacion-creada-con-el-fin-de-producir-menos-emisiones-de-carbono-en-el-desarrollo-de-software.html

### Terminadas / Paradas
* [OSCEdays](https://hablandoenvidrio.com/oscedays-conocimiento-abierto-economia-circular/)

## Empresas / Organizaciones
* https://www.reciclanet.org


## Relacionado

* [Datos abiertos](/mundolibre/docs/web/datosabiertos/)
    * [Charla: Datos abiertos, SL y medio ambiente](https://devtube.dev-wiki.de/videos/watch/7948bbf6-6967-426a-a390-078619e6d195)