---
title: "Utilidad: ffmpeg"
icon: fa-solid fa-sliders
linkTitle: "ffmpeg"
weight: 6
date: 2021-05-21
description: >
  Chuleta con procesos simples
categories: [Multimedia]
tags: [audio, video, podcast]
---

## Vídeo y Audio

```bash
alias ffmpeg='ffmpeg -hide_banner'
```

### Ver información
```bash
ffprobe -hide_banner -i "$INPUT" # Mostrar datos del fichero
ffmpeg -formats # show available formats
ffmpeg -codecs  # show available codecs
```

### Cortar sin recomprimir
https://trac.ffmpeg.org/wiki/Seeking
```bash
ffmpeg -i "$INPUT" -ss 00:06:19.000 -to 01:48:17.477000000 -c copy "c_$INPUT"
```

### Cambiar pista audio
* https://ochremusic.com/2016/07/05/replacing-video-audio-using-ffmpeg/
```bash
ffmpeg -i "$INPUT" -codec copy -an "sin_$INPUT" # Eliminamos las pistas de audio
ffmpeg -i "sin_$INPUT" -i AUDIO.flac -shortest -c:v copy -c:a aac -b:a 256k "a_$INPUT" 
# En un paso # https://superuser.com/questions/1137612/ffmpeg-replace-audio-in-video
ffmpeg -i "$INPUT" -i AUDIO.flac -c:v copy -map 0:v:0 -map 1:a:0 "a_$INPUT"
# Sin recomprimir el audio
ffmpeg -i "$INPUT" -i AUDIO.flac -c copy -map 0:v:0 -map 1:a:0 "a_$INPUT"
```

## Vídeo

### m3u8 online a fichero único
```bash
ffmpeg -i "$URL.m3u8" -c copy -bsf:a aac_adtstoasc "$OUTPUT.mp4"
```

### Añadir marca de agua/overlay
```bash
ffmpeg -i video.mp4 -i logo.png -filter_complex overlay=10:10 final.mp4
ffmpeg -i video.mp4 -i logo.png -filter_complex overlay=x=(main_w-overlay_w):y=(main_h-overlay_h) final.mp4
```

### VP9 Encode
https://trac.ffmpeg.org/wiki/Encode/VP9
```bash
ffmpeg -i "$INPUT" -c:v libvpx-vp9 -lossless 1 $OUTPUT.webm
```
* Para [Streaming]((/mundolibre/docs/multimedia/streaming/#vp9))

### HW Encode
* https://trac.ffmpeg.org/wiki/Hardware/VAAPI

* Si solo se va a usar un stream
```bash
ffmpeg -hwaccel vaapi -hwaccel_device /dev/dri/renderD128 -i
ffmpeg -init_hw_device vaapi=vaapi0:/dev/dri/renderD128 -filter_hw_device vaapi0
ffmpeg -vaapi_device /dev/dri/renderD128 # Simplificado, equivalente a la enterior
```
* Codificar
```bash
#  use the default decoder for some input, then upload frames to VAAPI and encode with H.264 and default setting
ffmpeg -vaapi_device /dev/dri/renderD128 -i "$INPUT" -vf 'format=nv12,hwupload' -c:v h264_vaapi -b:v 5M -maxrate 5M "c_$INPUT".mkv
#  If the input is known to be hardware-decodable, then we can use the hwaccel: 
ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -hwaccel_device /dev/dri/renderD128 -i "$INPUT" -c:v h264_vaapi "c_$INPUT".mkv
# when the input may or may not be hardware decodable we can do
ffmpeg -init_hw_device vaapi=foo:/dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -hwaccel_device foo -i "$INPUT" -filter_hw_device foo -vf 'format=nv12|vaapi,hwupload' -c:v hevc_vaapi "c_$INPUT".mkv
```


## Audio

### Normalización
* Obtener información con [ebur128](https://ffmpeg.org/ffmpeg-filters.html#ebur128)
```bash
ffmpeg -i "$INPUT" -af ebur128=peak=true:dualmono=true -f null - # Manera más rápida de medir
ffplay -f lavfi -i "amovie=$INPUT,ebur128=peak=true:dualmono=true:video=1:target=-16 [out0][out1]"  # Mostrar los datos en un vídeo
ffmpeg -i "$INPUT" -filter_complex "[0:a]ebur128=dualmono=true:target=-16:size=1280x720:video=1[v][a]" -map '[v]' -map '[a]' "$INPUT".webm # Guardar un vídeo con la gráfica
```
* Normalización con [loudnorm](https://ffmpeg.org/ffmpeg-filters.html#loudnorm), más información en el artículo de [Audacity](/mundolibre/docs/multimedia/audacity/#loudness-normalization) o en el [script](https://gitlab.com/Marzal/dmc-scripts/-/tree/master#dmc_fflufssh)
```bash
ffmpeg -i "$INPUT" -af loudnorm=I=-16:TP=-1:LRA=7:dual_mono=true:print_format=json -f null - # Medidas para normalización con 2 pasadas

#Segunda pasada usando los datos anteriores
ffmpeg -i "$INPUT" -af "loudnorm=I=$I:tp=$TP:LRA=$LRA:measured_I=$m_I:measured_TP=$m_TP:measured_LRA=$m_LRA:measured_thresh=$m_TH:offset=$m_o:dual_mono=true:linear=true:print_format=json" -ar "$HZ" ""LUFS_$INPUT" 2>&1
```

* Ver opciones de los filtros
```bash
ffmpeg -help filter=ebur128 # Usa 48kHz para funcionar
ffmpeg -help filter=loudnorm
```

### Audiogramas
```bash
INPUT=adudio.ogg; IMAGEN=fondo.png; RESOLUCION=1280x720; COLOR=blue; ALTURAONDA=-200
ffmpeg -i "$INPUT" -i "$IMAGEN" -filter_complex "[0:a]showwaves=s=$RESOLUCION:mode=cline:colors=$COLOR[sw];[1][sw]overlay=x=W-w-0:y=$ALTURAONDA:format=auto,format=yuv420p[v]" -map "[v]" -map 0:a -movflags +faststart -c:a libvorbis -ab 128k "$INPUT".webm
```
* Docu
  * https://ffmpeg.org/ffmpeg-filters.html#showwaves
  * https://trac.ffmpeg.org/wiki/Waveform
  * https://salmorejogeek.com/2020/11/08/como-crear-un-video-waveform-a-partir-de-una-imagen-y-un-archivo-de-audio-usando-ffmpeg-desde-la-terminal-en-linux/


### Capitulos
* Hay que crear un fichero con los metadatos con el siguiente formato
```ini
;FFMETADATA1
title=Residuo Cero
artist=David Marzal
;this is a comment

;para OGG
CHAPTER000=00:00:00.000
CHAPTER000NAME=Intro
CHAPTER001=00:00:49.000
CHAPTER001NAME=Capitulo1

;para MP3 y otros
[CHAPTER]
TIMEBASE=1/1
START=0
#chapter ends at 0:01:00 = 60 segundos con 1/1
END=
#Necesari poner el END para que funcione el mapeo a Vorbis Comment (OGG)
title=Capitulo 1

[CHAPTER]
TIMEBASE=1/1
START=60
END=999
#En el ultimo SI que hay que ponerlo en ambos
title=Capitulo final
```
* Ejemplo de ocmo usarlo para añadirlos a un audio sin modificar nada más. 
```bash
ffmpeg -i "$INPUT" -i FFMETADATAFILE -map_metadata 1 -c copy OUTPUT
```
* Docu
  * https://www.ffmpeg.org/ffmpeg-all.html#Metadata-1
  * https://stackoverflow.com/questions/49364269/how-to-add-chapters-to-ogg-file
  * [Ejemplo de uso en un script](https://gitlab.com/Marzal/dmc-scripts/-/tree/master#dmc_fflufssh)

### Convertir a WAV para whisper.cpp
```bash
ffmpeg -i "$INPUT" -ar 16000 -ac 1 -c:a pcm_s16le ${INPUT%.*}.wav
```

## Documentación
* https://www.emezeta.com/articulos/como-usar-ffmpeg-para-editar-video
* https://trac.ffmpeg.org/wiki/TheoraVorbisEncodingGuide