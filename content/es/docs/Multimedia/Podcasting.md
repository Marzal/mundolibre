---
title: "Podcasting"
icon: fa-solid fa-podcast
linkTitle: "Podcasting"
weight: 5
date: 2021-06-07
description: >
  Información para publicar un podcast de manera libre
categories: [Multimedia]
tags: [DIY,audio,podcast,feed]
---
## Buscadores / Indices
* https://www.listennotes.com
* https://www.stitcher.com/search
* Docu
  * https://lifehacker.com/the-best-podcast-search-engine-1818560337


## Feed
* En el artículo de [Feed RSS en Hugo](/mundolibre/docs/web/rss/) están las tripas y más info
* <https://podcastindex.org>
  * [Namespace](https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md)
* Funkwhale:
  * Es mejorable, ademas convierte a mp3 para el feed.
  * Código [renderers](https://dev.funkwhale.audio/funkwhale/funkwhale/-/blob/develop/api/funkwhale_api/audio/renderers.py), [serializers](https://dev.funkwhale.audio/funkwhale/funkwhale/-/blob/develop/api/funkwhale_api/audio/serializers.py)
* Archive.org
  * Se puede construir dinamicamente con la busqueda avanzada
  ```html
      https://archive.org/advancedsearch.php?q=creator%3A(KDE_express)&fl[]=title&fl[]=publicdate&fl[]=description&fl[]=downloads&fl[]=identifier&fl[]=item_size&fl[]=licenseurl&fl[]=subject&fl[]=format&fl[]=mediatype&fl[]=language&sort[]=publicdate+desc&sort[]=&sort[]=&rows=999&page=1&callback=callback&save=yes&output=rss
  ```

## Recursos
* Stats
  * [How to understand podcast stats](https://podnews.net/article/understanding-podcast-statistics)
  * PI
    * [Generador de gráficas con datos de podcastindex.org](https://podcastdata.org/)
    * [Podcasting 2.0 namespace trends](https://griddlecakes.com/nstrends/)
    * [Podcast Index Statistics, visualized](https://livewire.io/podcast-index-stats-visualized/)
* Creador de feeds 2.0
  * https://sovereignfeeds.com/ - [Open Source](https://github.com/thebells1111/sovereign-feeds)
* Varios
  * https://podcast-privacy.com/
  * https://podtk.dev/api/parse?url=https://kdeexpress.gitlab.io/feed


## Alojamiento
* Con [Podcasting 2.0](https://podcastindex.org/apps?appTypes=hosting) 
* Métricas certificadas por IAB Tech Lab
  * [Blubrry](https://www.blubrry.com/) - 12 $ - Buen Pod 2.0
  * Captivate - 17 € - Buen Pod 2.0
  * [Libsyn](https://www.libsyn.com/) - 5 $ - Mal Pod 2.0
  * [Spreaker (VoxNest)](https://www.spreaker.com/) - 5h total o 10 episodios -> 7 € - Mal Pod 2.0
  * [Podbean](https://www.podbean.com/podcast-hosting-pricing)) - 5h de almacenamiento total -> 9 $ - Nada de Pod 2.0
  * Whooskha - 29€ /mes - Nada de Pod 2.0
* Sin certificar
  * [Castopod](https://castopod.org/) - FLOSS y Buen Pod 2.0
  * Worpress con [PowerPress by Blubrry](https://blubrry.com/services/powerpress-plugin/) - FLOSS y Buen Pod 2.0
  * [PeerTube](https://joinpeertube.org/) + plugin - FLOSS y Buen Pod 2.0
  * [Ivoox](https://www.ivoox.com/) - Gratis (feed 20 últimos) -> 7€ (Feed ilimitado) - Nada de Pod 2.0

### Autopublicaciones
Podcast distribution (No se recomienda!)
* [Anchor](https://help.anchor.fm/hc/en-us/articles/360004954351-How-Anchor-s-one-click-distribution-works) : Spotify, Apple Podcasts, Breaker, Castbox, Google Podcasts, Overcast, Pocket Casts, RadioPublic


## Directorios Sindicables con RSS
* [iVoox](https://www.ivoox.com/podcasters)
* [Spotify](https://podcasters.spotify.com/) : Requiere email en el RSS para verificación 
  * [Condiciones](https://www.spotify.com/us/legal/end-user-agreement/)
* [Google Podcast](https://podcastsmanager.google.com/about?hl=es)
  * https://pubsubhubbub.appspot.com/
* [Apple Podcasts](https://itunesconnect.apple.com/) : requiere email en el RSS
  * No acepta canal alfa en el logo
* Amazon
* Breaker, Castbox, Overcast, Pocket Casts, RadioPublic, Anchor...

### Documentación
* https://viapodcast.fm/donde-subir-y-alojar-tu-podcast/
* https://liberaturadio.org/radio-wordpress-el-podcast/


## Formato de audio

### ogg Vorbis
* No valido para Apple Podcast y algunas apps
* Formato
  * CBR 96kbps mono es suficiente -> Preset 5 en Audacity
  * 44.1khz / 48khz
* [Spotify](https://www.soundguys.com/spotify-review-25680/) - [Settings](https://support.spotify.com/us/article/audio-quality/)


## Etiquetas

### [ID3](https://en.wikipedia.org/wiki/ID3)
* [id3v2.4.0-frames](https://id3.org/id3v2.4.0-frames)
* [ID3 v2.3 Tags](https://id3.org/id3v2.3.0)
  * Siempre: title(TIT2), artist(TPE1), album(TALB), year(TYER)*, genre y "cover art", Language(TLAN)=(spa)
  * Recomendable: Comments(COMM), Enconded by(TENC), Copyright(TCOP)=(YYYY CC-BY-ND), URL
  * Otros: Composer(TCOM)
* Extended
  * Interesantes: Subtitle(TIT3), Released(TDRL)
  * Conductor(TPE3), Lyricist(TEXT), Publisher(TPUB), Orchestra(TPE2), Content(TCON), ISRC, Recorded(TDRC), Initial key(TKEY), BPM(TBPM), Artist URL(WOAR)
* Podcast Tags:
  * Identifier : Lo suele poner iTunes
  * Feed
  * Description
* Telegram
  * Android [audioinfo/mp3](https://github.com/DrKLO/Telegram/blob/master/TMessagesProj/src/main/java/org/telegram/messenger/audioinfo/mp3/ID3v2Info.java)
    * APIC, COMM, TALB, TCMP, TCOM, TCON, TCOP, TDRC, TLEN, TPE1, TPE2, TPOS, TRCK, TIT1, TIT2, TYER, USLT
  * Android [exoplayer2/mp4](https://github.com/DrKLO/Telegram/blob/master/TMessagesProj/src/main/java/com/google/android/exoplayer2/extractor/mp4/MetadataUtil.java)
    * TIT2, TCOM, TDRC, TPE1, TSSE, TALB, USLT, TCON, TIT1
    * TPOS, TRCK, TBPM, TCMP, TPE2, TSOT, TSOT2, TSOA, TSOP, TSOC, ITUNESADVISORY, ITUNESGAPLESS, TVSHOWSORT, TVSHOW
  * Desktop
    * TPE1 - TIT2
* [Funkwhale](https://dev.funkwhale.audio/funkwhale/funkwhale/-/blob/develop/api/funkwhale_api/music/metadata.py)
  * OggVorbis
    * "position": TRACKNUMBER
    * "disc_number": DISCNUMBER
    * "album_artist": albumartist
    * "date": date
    * "comment": comment
  * MP3
    * "position": TRCK
    * "disc_number": TPOS
    * "title": TIT2
    * "artist": TPE1
    * "artists": ARTISTS
    * "album_artist": TPE2
    * "album": TALB
    * "date": TDRC
    * "genre": TCON
    * "mbid": UFID           
    * "license": WCOP
    * "copyright": TCOP
    * "comment": COMM
* [VLC](https://wiki.videolan.org/ID3/)
  * Title(TIT2), Artist(TPE1), Date(TDRC), Album(TALB), Track number(TRCK), Genre(TCON), Language(TLAN), Now Playing, Copyright(TCOP), Publisher(TPUB/ORGANIZATION), Encoded by(TENC/encoder), Description (?/comments+DESCRIPTION)
* Equivalencias/ Mapeos
  * [Hydrogenaud](https://wiki.hydrogenaud.io/index.php?title=Tag_Mapping#Mapping_Tables)
  * [Kid3](https://docs.kde.org/trunk5/en/kid3/kid3/commands.html#table-frame-list)
  * [MP3tag](https://help.mp3tag.de/main_tags.html)
* ffmpeg
  * ID3v2.4 by [default](https://ffmpeg.org/ffmpeg.html) - [Source Code](https://github.com/FFmpeg/FFmpeg/blob/master/libavformat/id3v2.c)
  * [mp3](https://www.ffmpeg.org/ffmpeg-formats.html#mp3)
    * album(TALB), composer(TCOM), genre(TCON), copyright(TCOP), encoded_by(TENC), title, language(TLAN), artist(TPE1), album_artist, performer, disc, publisher, track, encoder, lyrics
    * compilation, date(TDRC), creation_time, album-sort, artist-sort, title-sort
  * ogg
    * DESCRIPTION sobreescribe a comment
  * Docu
    * [Metadata API](https://www.ffmpeg.org/doxygen/3.4/group__metadata__api.html) - [Source Code](https://github.com/FFmpeg/FFmpeg/blob/master/libavformat/avformat.h#L373)
    * https://wiki.multimedia.cx/index.php/FFmpeg_Metadata
  * Ejemplos
    * https://gist.github.com/eyecatchup/0757b3d8b989fe433979db2ea7d95a01
* Articulos
  * https://prestopod.com/how-to-add-id3-tags-to-your-podcast-mp3-file/

### Vorbis comment
* [Especificación](https://xiph.org/vorbis/doc/v-comment.html)
  * Propone: TITLE, ALBUM, TRACKNUMBER, ARTIST, COPYRIGHT, LICENSE, ORGANIZATION , DESCRIPTION, GENRE, DATE, LOCATION, CONTACT
* ffmpeg
  * [libavformat/vorbiscomment.c](https://github.com/FFmpeg/FFmpeg/blob/master/libavformat/vorbiscomment.c)
    * ALBUMARTIST = album_artist
    * TRACKNUMBER = track
    * DISCNUMBER = disc
    * DESCRIPTION = comment
* VLC
  * [modules/demux/xiph_metadata.c](https://github.com/videolan/vlc/blob/master/modules/demux/xiph_metadata.c)
    * TITLE, ALBUM, TRACKNUMBER, ARTIST (_PERFORMER_), COPYRIGHT, _LICENSE_, _ORGANIZATION_ (Publisher), DESCRIPTION (COMMENT-COMMENTS), GENRE, DATE, _LOCATION_, _CONTACT_
    * ENCODER (EncodedBy), TRACKTOTAL-TOTALTRACKS, RATING, CHAPTER
* Recomendaciones
  * https://wiki.hydrogenaud.io/index.php?title=Vorbis_comment
  * https://www.legroom.net/2009/05/09/ogg-vorbis-and-flac-comment-field-recommendations
    * ENCODED-BY, COMMENT

### Capitulos
* Se pueden añadir usando [ffmpeg](/mundolibre/docs/multimedia/ffmpeg#capitulos)
  * [OGG](https://wiki.xiph.org/Chapter_Extension): No coge los [CHAPTERS] como el resto, hay que añadir sus CHAPTER###
* Reproductores
  * VLC 
    * En **OGG** funcionan en la 3.0.
    * Los soportará en **MP3** en la [4.0](https://code.videolan.org/videolan/vlc/-/issues/7485)
  * AntennaPod
    * [parser/media/vorbis/VorbisCommentChapterReader.java](https://github.com/AntennaPod/AntennaPod/blob/master/parser/media/src/main/java/de/danoeh/antennapod/parser/media/vorbis/VorbisCommentChapterReader.java)
    * [parser/media/id3/ChapterReader.java](https://github.com/AntennaPod/AntennaPod/blob/master/parser/media/src/main/java/de/danoeh/antennapod/parser/media/id3/ChapterReader.java)
