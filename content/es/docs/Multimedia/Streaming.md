---
title: "Video Streaming"
icon: fa-solid fa-circle-play
linkTitle: "Streaming"
weight: 4
date: 2020-06-17
description: >
  Codecs, aplicaciones y plataformas
categories: [Multimedia]
tags: [DIY,video]
---

## CODECS
* [Listado de codecs abiertos](https://en.wikipedia.org/wiki/List_of_open-source_codecs)

### Audio
* Info 
  * <https://en.wikipedia.org/wiki/Comparison_of_audio_coding_formats>
* Compatibilidades
  * [Ogg][ogg] ([Opus][opus], [Vorbis][vorbis], [FLAC][flac], [Speex](https://en.wikipedia.org/wiki/Speex), MP3 ...)
  * [mkv][mkv] ([Opus][opus], [Vorbis][vorbis], [FLAC][flac], MP3, ...[37 o más][mkv_spec])
  * [WebM][webm] (**[Opus][opus]**, [Vorbis][vorbis]) - [Spec](https://www.webmproject.org/docs/container/)
  * MP3\*[Caducada patente](https://en.wikipedia.org/wiki/MP3#Licensing,_ownership,_and_legislation)
* HTML5 audio element
  * <https://en.wikipedia.org/wiki/HTML5_audio>
  * <https://caniuse.com/audio>
  * PCM/WAV (-{IE,Opera}), MP3, Opus(-{IE,Apple}), Vorbis(-IE,%Apple), ACC(%Firefox), FLAC
### Video
* Info
  * <https://en.wikipedia.org/wiki/Comparison_of_video_container_formats> -> MKV o webm
  * https://en.wikipedia.org/wiki/Comparison_of_video_codecs
  * https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Video_codecs -> WebM(VP9+Opus) / WebM(AV1+Opus)
* Compatibilidades
  * [Ogg][ogg] ([Theora][theora], [Daala][daala], [Dirac](https://en.wikipedia.org/wiki/Dirac_(video_compression_format)), mp4 ... )
  * [mkv][mkv] ([AV1][av1], [VP9][vp9], [Daala][daala], [Theora][theora], ... [21 o más][mkv_spec])
  * [WebM][webm] ([AV1][av1], **[VP9][vp9]**, VP8)
* HTML5 video element
  * <https://en.wikipedia.org/wiki/HTML5_video#Browser_support> -> WebM(VP9)
  * https://caniuse.com/video
  * WebM (-IE,#Safari_iOS), MPEG4/H.264, Ogg/Theora(-{IE, Apple, Android, ...}), WebVTT, HEVC/H.265 (-{Firefox,Chrome, Android},%IE)

[mkv]: https://en.wikipedia.org/wiki/Matroska "Contenedor"
[mkv_spec]: https://matroska.org/technical/codec_specs.html "Codec Mappings"
[ogg]: https://en.wikipedia.org/wiki/Ogg "Contenedor"
[WebM]: https://en.wikipedia.org/wiki/WebM "Contenedor"
[Vorbis]: https://en.wikipedia.org/wiki/Vorbis "Audio Codec"
[FLAC]: https://en.wikipedia.org/wiki/FLAC "Audio Codec"
[opus]: https://en.wikipedia.org/wiki/Opus_(codec) "Audio Codec"
[Theora]: https://en.wikipedia.org/wiki/Theora "Video Codec"
[VP9]: https://en.wikipedia.org/wiki/VP9 "Video Codec"
[AV1]: https://en.wikipedia.org/wiki/AV1 "Video Codec"
[Daala]: https://en.wikipedia.org/wiki/Daala "Video Codec"

## FFMPEG
[StreamingGuide](https://trac.ffmpeg.org/wiki/StreamingGuide)
### VP9

* <https://trac.ffmpeg.org/wiki/Encode/VP9>

```bash
ffmpeg -h encoder=libvpx-vp9 # Mostrar opciones

-c:v libvpx-vp9 -b:v 2500k -c:a libopus -b:a 128k -ar 44100
#-framerate 25
```
* >Para Streaming
```bash
-qmin 4 -qmax 48 -quality realtime -speed 5 -static-thresh 0 -max-intra-rate 300 -error-resilient 1 \ #libvpx
-tile-columns 2 -frame-parallel 1 -row-mt 1 \ #VP9
-threads 5 -f webm output.webm # ffmpeg
```

* <https://developers.google.com/media/vp9/live-encoding>
```bash
-r 25 -g 75 -s 1280x720 -quality realtime -speed 5 -threads 5 -row-mt 1 -tile-columns 2 -frame-parallel 1 -qmin 4 -qmax 48 -b:v 3000k
```

* <https://developers.google.com/media/vp9/bitrate-modes/>
```bash
-minrate 2000k -maxrate 23000k #CBR
-crf 32 #CQ
```

* Ejemplo completo CBR streaming

```bash
-c:v libvpx-vp9 -b:v 2000k -c:a libopus -b:a 128k -ar 48000 -minrate 2000k -maxrate 2000k -qmin 4 -qmax 48 -quality realtime -speed 8 -static-thresh 0 -max-intra-rate 300 -error-resilient 1 -tile-columns 1 -frame-parallel 1 -row-mt 1 -threads 5 -f webm output.webm
```

### libva / VAAPI
* <https://wiki.archlinux.org/index.php/Hardware_video_acceleration>
* <https://wiki.libav.org/Hardware/vaapi>
* <http://www.ffmpeg.org/ffmpeg-codecs.html#VAAPI-encoders>
  * <https://trac.ffmpeg.org/wiki/Hardware/VAAPI>
  * <https://trac.ffmpeg.org/wiki/Hardware/QuickSync>

```Bash
vainfo --display drm --device /dev/dri/renderD128
ffmpeg -hide_banner -hwaccels
ffmpeg -hide_banner -h encoder=vp9_vaapi
for i in encoders decoders filters; do
  echo $i:; ffmpeg -hide_banner -${i} | grep -i "vaapi"
done
```

```Bash
# Encode
ffmpeg -vaapi_device /dev/dri/renderD128 -i input.mp4 -vf 'format=nv12,hwupload' -c:v vp9_vaapi \
-b:v 3M -maxrate:v 3M -minrate:v 3M vp9_output.webm

ffmpeg -vaapi_device /dev/dri/renderD128 -i input.mp4 -vf 'format=nv12,hwupload' -c:v vp9_vaapi \
-global_quality 50 -bf 2 -bsf:v vp9_raw_reorder,vp9_superframe output.webm

ffmpeg -vaapi_device /dev/dri/renderD128 -re -i input.mp4 -vf 'format=nv12,hwupload' -c:v vp9_vaapi \
-g 150 -compression_level 1 -bf 4 -bsf:v vp9_raw_reorder,vp9_superframe -b:v 3M -maxrate:v 3M -minrate:v 3M vp9_output.webm
# -re # Solo leyendo de ficheros para coger su framerate

# Transcode completamente en HW
ffmpeg -vaapi_device /dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -i input.mp4 \
-c:v vp9_vaapi -b:v 3M vp9_output.webm

# TC HW + Si no sabemos el formato de la fuente
ffmpeg -vaapi_device /dev/dri/renderD128 -hwaccel vaapi -hwaccel_output_format vaapi -i input.mp4 \
-vf 'format=nv12|vaapi,hwupload' \
-c:v vp9_vaapi -b:v 3M vp9_output.webm
```
* <https://www.reddit.com/r/VP9/comments/g9uzzv/hardware_encoding_vp9_on_intel/>
* <https://stackoverflow.com/questions/55007246/ffmpeg-vp9-vaapi-encoding-to-a-mp4-or-webm-container-from-given-official-ffm>
* <https://stackoverflow.com/questions/45476554/ffmpeg-hwaccel-error-with-hwupload>
* <https://gist.github.com/Brainiarc7/95c9338a737aa36d9bb2931bed379219>

## Stream Servers (self-hosted)
### Owncast
* Info
  * <https://owncast.online/>
* Instalación
  * `curl -s https://owncast.online/install.sh | bash`
  * [Docker](/mundolibre/docs/containers/docker_apps/#owncast)
* Config
  * rtmp://localhost/live
    * http://localhost:8080
    * http://localhost:8080/admin
  * Por [defecto](https://github.com/owncast/owncast/blob/master/config/defaults.go)
    * StreamQuality
      * IsAudioPassthrough: true
      * VideoBitrate: 1200
      * FPS: [24](https://owncast.online/docs/encoding/#framerate)
      * Encoder preset : [veryfast](https://owncast.online/docs/encoding/#encoder-preset)
  * Ficheros a modificar
    * `config-default.yaml` -> `config.yaml`
  * [Broadcasting software](https://owncast.online/docs/broadcasting/)
    * Video : x264
* Debug
`owncast -enableVerboseLogging -enableDebugFeatures`

* FirewallD:
```bash
FDSDIR='/etc/firewalld/services'
FDSOWNCAST="$FDSDIR/owncast.xml"

if [[ -d $FDSDIR ]] && [[ ! -f $FDSOWNCAST ]] ; then
  cat <<'EOF' > $FDSOWNCAST
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>owncast</short>
  <description>Streaming (RTMP-1935) + chat out of the box (TCP-8080)</description>
  <port protocol="tcp" port="8080"/>
  <port protocol="tcp" port="1935"/>
</service>
EOF
  firewall-cmd --permanent --zone=home --add-service=owncast
  firewall-cmd --permanent --zone=home --add-service=http --add-service=https # Si usamos Traefik / proxy inverso
  systemctl restart firewalld.service
  systemctl --no-pager --full status firewalld.service
else
  echo "O no exite $FDSDIR"
  echo "O ya exite $FDSOWNCAST"
fi
```

### Open Streaming Platform
* Info
  * <https://openstreamingplatform.com/>
* Instalación
  * [Docker](https://hub.docker.com/r/deamos/openstreamingplatform) : `docker pull deamos/openstreamingplatform`
    * <https://gitlab.com/Deamos/open-streaming-platform-docker>

## Stream Services (self+hosted)

### PeerTube
* Instancias: [Buscador](https://joinpeertube.org/instances#instances-list) - [Listado](https://instances.joinpeertube.org/instances)
  * Desde la 3.0 se pueden hacer directos
  * Usa [HLS player with p2p-media-loader](https://github.com/Chocobozzz/PeerTube/pull/3250)
* Configuración de OBS como el resto H264 High 4.1 MP4....
  * Protocolo ingesta : [solo](https://github.com/Chocobozzz/PeerTube/pull/3250) RTMP

### autistici.org
* Info
  * <https://live.autistici.org/>
  * <https://git.autistici.org/ai3/tools/live>

* rtmp://live.autistici.org/ingest/
  * x264 sencillo -> OK
  * ogg(Theora+Vorbis) -> OBS no deja (Function not implemented)
  * webm/mkv(VP8+Vorbis) -> OBS no deja (Function not implemented)
  * webm(VP9+Opus) -> OBS no deja (Function not implemented)

* iframe
```html
<p><iframe src="https://live.autistici.org/#gnulinuxvalenciasocial" width="800" height="600" frameborder="0" allowfullscreen="allowfullscreen"><span style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" data-mce-type="bookmark" class="mce_SELRES_start">﻿</span></iframe></p>
```

### G.I.S.S

* <http://giss.tv/>
* gissnetwork@gmail.com
* <http://giss.tv:8000/my_channel.ogg>
* <http://giss.tv/wiki/index.php/Streaming_Tools>

### SRT Protocol (verde aún)

<https://obsproject.com/wiki/Streaming-With-SRT-Protocol>

### Services

#### SRT Live Server (free, open source)

<https://github.com/Edward-Wu/srt-live-server>

#### Nimble Streamer (free, closed source)

* <https://wmspanel.com/nimble/live_streaming>
* <https://wmspanel.com/nimble/codecs>

VP8/9 = RTSP

## Configuraciones
* https://www.dacast.com/support/knowledgebase/live-encoder-configuration/
  * H.264 Profile : High 
* https://manycam.com/blog/best-live-streaming-settings/
  * 1080p 30FPS : H.264 4.1 Main/High
  * 1440p 30FPS : H.264 5.0 Main/High
* https://obsproject.com/forum/resources/low-latency-high-performance-x264-options-for-for-most-streaming-services-youtube-facebook.726/

### Youtube
* https://support.google.com/youtube/answer/4603579?hl=es
* Directos : https://support.google.com/youtube/answer/2853702?hl=es
  * 720p 30FPS: 4 Mbps - 3000-8000 Kbps (Min/Max para AV1/H.265)
  * 1080p 30FPS: 10 Mbps - 3000-8000 Kbps
  * 1440p 30FPS: **15 Mbps** - 5000-25000 Kbps 
  * Codec: 
    * CBR - H.264 de nivel 4.1 (Main/**High**) para 1080p como máximo y a 30 FPS
    * CBR - AV1 / H.265 (HEVC) (recomendado)
  * Audio : **AAC** o MP3 a 128-384 Kbps en estéreo 44,1-48 KHz
  * OBS:
    * Espacio de color:	Rec. 709 para SDR / **HLG** o 2100 PQ para HDR
    * Profundidad de bits: 8 SDR / P010 HDR (H.265 HEVC)

## Archive.org

### Derivates
<https://help.archive.org/help/category/archive-org/files-formats-and-derivatives/>

> What encoding specifications are best for .mp4 files?
* <https://help.archive.org/help/movies-and-videos-a-basic-guide/>
* For your original mp4 to work in the online player we currently require the file to have:
```YAML
audio: aac
video: h.264
moov atom: front
pixel format: yuv420p
```
>If this is not the case we will derive a new mp4 with the file name suffix .ia.mp4