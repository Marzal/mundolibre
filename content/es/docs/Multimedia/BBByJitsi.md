---
title: "Videconferencias con BBB o Jitsi"
icon: fa-solid fa-headset
linkTitle: "BBB-Jitsi"
weight: 10
date: 2023-03-09
description: >

categories: [Multimedia]
tags: [audio, video, streaming, self-hosted]
---

## BBB
* https://docs.bigbluebutton.org/2.4/install.html

### Instalación
* Requisitos
    * Ubuntu 20.04
    * IPv4+IPv6 o workaround para el IPv6
    * FQDNs : El FQDN para IPv6 no es estrictamente necesario
    * Firewall externo abierto:
        * 22/tcp, 80/tcp, 443/tcp and 16384:32768/udp

#### Script
```bash
wget -qO- https://raw.githubusercontent.com/bigbluebutton/bbb-install/v2.7.x-release/bbb-install.sh | bash -s -- -w -v focal-270 -s fqdn.servidor.web -e correo@electronico.es -g
# Probar
apt-get purge bbb-demo
```

#### Checks
```bash
bbb-conf --check
bbb-conf --status
```

#### Greenlight
* Crear los usuarios administradores que hagan falta
```bash
docker exec greenlight-v3 bundle exec rake user:create["Nombre","correo@electronico.es","Passw0rd","admin"]
```

### Actualización
* https://wiki.fsfe.org/TechDocs/TechnicalProcesses/BigBlueButton
```bash
# Save old BBB configuration
bbb-conf --check > $(date +%F)-pre-update-conf.txt

# Update and upgrade
apt update && apt upgrade

# Set IP for server
bbb-conf --setip $FQDN

# Save new and compare BBB config
bbb-conf --check > $(date +%F)-post-update-conf.txt
diff $(date +%F)-pre-update-conf.txt $(date +%F)-post-update-conf.txt
```

### Optimización
* https://github.com/manishkatyan/bbb-optimize
* https://gitlab.com/MaadiX/bbb-tuning/-/wikis/Big-Blue-Button-Custom-settings

### Documentacion
* https://higheredlab.com/bigbluebutton-guide/

### Streaming

#### BigBlueButton-liveStreaming
Permite emitir y grabar la sala.
* [Github](https://github.com/aau-zid/BigBlueButton-liveStreaming)
* VARIABLES
    * __BBB_MEETING_ID__ - ID of the BBB Meeting (You can get the ID via an API call: https://$BBB_SERVER/bigbluebutton/api/getMeetings?checksum=)
    * __BBB_SECRET__ : $(bbb-conf --secret)
* Requisitos:
    * `pacman -S libxcrypt-compat` 
##### Docker compose
* env_file
```bash
# https://github.com/aau-zid/BigBlueButton-liveStreaming
# URL to BBB including http/https:
BBB_URL=https://$BBB_SERVER/bigbluebutton/api
# Secret of your BBB installation:
BBB_SECRET=
# BigBlueButton meetingID:
BBB_MEETING_ID=
# URL of your streaming server including rtmp. Leave out to disable streaming:
BBB_STREAM_URL=rtmp://

#### Optional settings
# Timezone (default: Europe/Vienna):
TZ=Europe/Madrid
# the username to join the meeting. (Default: Live)
BBB_USER_NAME=Peertube

# hide the meeting title in the top bar (Default: false)
#BBB_HIDE_MEETING_TITLE=true
# hide the annotation who is currently talking (Default: false)
#BBB_HIDE_WHO_TALKS=true
# override background color by a CSS color, e.g., "black" or "#ffffff"
#BBB_BACKGROUND_COLOR
# add a logo to the video, passed as an image URL (Default: none/disabled)
#BBB_LOGO_URL=https://
# corner where to place the logo: "top/left", "top/right", "bottom/left" or "bottom/right" (Default "top/right")
#BBB_LOGO_POS=bottom/right

# start meeting
BBB_START_MEETING=false
# attendee password (optional - has to be set to the attendee password of moodle/greenlight or any other frontend to allow joining via their links):
#BBB_ATTENDEE_PASSWORD=
# moderator password (optional - has to be set to the moderator password of moodle/greenlight or any other frontend to allow joining via their links):
#BBB_MODERATOR_PASSWORD=
# meeting title (optional - only works if the meeting is started by the liveStreaming):
#BBB_MEETING_TITLE=KDE Express
# Message to post in BBB Chat when joining a conference. No funciona si requiere aprobación el usuario
#BBB_CHAT_MESSAGE=Buenas, tambien estamos emitiendo esta charla en https://tube.kockatoo.org/c/kde/videos
# download / save BigBlueButton meeting in lossless mkv format
BBB_DOWNLOAD_MEETING=true
# play intro file (can be a local file in videodata folder e.g. /video/intro.mp4 or a url of a mediastream e.g. https://my.intro.stream)
#BBB_INTRO=
# begin the intro at position (optional, e.g. 00:00:05)
#BBB_BEGIN_INTRO_AT=04:40
# end intro after (optional, e.g. 01:00:00 - after one hour)
#BBB_END_INTRO_AT=
# Resolution to be streamed/downloaded in format WxH (default 1920x1080)
#BBB_RESOLUTION=1920x1080

#### Chat settings
# show chat in live stream
BBB_SHOW_CHAT=false
# Enable Chat feedback channel
#BBB_ENABLE_CHAT=true
# Set REDIS host (default: 'redis')
#BBB_REDIS_HOST=redis
# Set REDIS channel to subscribe (default: 'chat')
#BBB_REDIS_CHANNEL=chat
# the username to join the meeting for chatting. (Default: Chat)
#BBB_CHAT_NAME=ChatStreamer
#  The URL of the stream that should be displayed in the chat (Default: rtmp:// destination)
#BBB_CHAT_STREAM_URL=

#### Advanced Usage
# video bitrate (in k) (Default: 4000)
#FFMPEG_STREAM_VIDEO_BITRATE=5000
# number of threads for ffmpeg during streaming (Default: 0 (auto))
#FFMPEG_STREAM_THREADS=0
# ffmpeg options to use when downloading, see `stream.py` for default value
#FFMPEG_DOWNLOAD_OPTIONS=
# whether to disable `/dev/shm` usage of the browser (default: false),
# BROWSER_DISABLE_DEV_SHM_USAGE
```
* docker-compose.yml
```docker
version: '3.5'
services:
  bbb-streamer:
    container_name: liveStreamingName
    shm_size: '2gb'
    env_file: .env
    build: 
        context: ./
    volumes:
      - ./videodata:/video
```
* Tras configurar, lanzar:
```bash
docker-compose up -d
docker-compose down
```
* [API MATE](https://mconf.github.io/api-mate/)
    * https://bbb./bigbluebutton/api/getMeetings?checksum=f24b829f44ab07c861f0bfca32e5f6e6a6d45df4

#### bbb-broadcaster
* https://github.com/oxzi/bbb-broadcaster

### Exportar grabaciones
Para descargar la sesión desde
#### Cliente
* Docker
    * https://hub.docker.com/r/th3r3alduk3/bbb-dl - https://github.com/C0D3D3V/bbb-dl
    * https://github.com/jibon57/bbb-recorder
    * https://github.com/trahay/bbb-downloader
* CLI
    * https://github.com/MrKinau/bbb-downloader
#### Servidor BBB
* Docker
    * https://github.com/tilmanmoser/bbb-video-download
* CLI
    * https://github.com/danielpetri1/bbb-recording-exporter - Client: https://github.com/danielpetri1/bbb-recording-exporter/tree/client-side


## Jitsi

### LOGS

<https://wiki.archlinux.org/index.php/Jitsi-meet#Log_evaluation>

```Bash
tail -f /var/log/jitsi/j*.log /var/log/prosody/prosody.{err,log} | grep -v AbstractHealthCheckService
```

Ver salas creadas y cerradas:

```Bash
grep -E 'Created|Disposed' /var/log/jitsi/jicofo.log | cut -d" " -f2,3,7,11,14
```

Las IPs aparecen en /var/log/jitsi/jvb.log

```Bash
journalctl --unit jicofo.service | grep "Created new focus" | cut -d" " -f7,8,16

journalctl --unit jicofo.service | grep "Disposed conference for room" | cut -d" " -f7,8,16
```

### ESTADÍSTICAS

<https://github.com/jitsi/jitsi-videobridge/blob/master/doc/statistics.md>

<https://github.com/jitsi/jitsi-videobridge/blob/master/doc/rest.md>

<https://www.callstats.io/integrate-jitsi/>

<https://www.callstats.io/integrate/jitsi-videobridge/>

<https://community.jitsi.org/t/monitoring-of-jitsi-meet/28226/19>

<https://community.jitsi.org/t/command-or-log-file-for-jitsi-server-status/27722>

<https://github.com/haidlir/jitsi-monitoring>