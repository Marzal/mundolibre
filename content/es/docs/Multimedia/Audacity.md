---
title: "Programa: Audacity"
icon: fa-solid fa-volume-high
linkTitle: "Audacity"
weight: 3
date: 2021-05-25
description: >
  Grabación y edición de un podcast
categories: [Multimedia]
tags: [sonido,podcast]
---

## Instalación
* El AppImage NO soporta JACK.
    * Ademas ahora misno no funciona en Arch, pero proximamente dicen que si.
* La versión de [Flatpak](https://github.com/flathub/org.audacityteam.Audacity) NO lleva los [plugins](https://github.com/flathub/org.freedesktop.LinuxAudio.BaseExtension) por defecto ni los instala como dependencia.
    * [org.freedesktop.LinuxAudio.Plugins.swh](https://github.com/flathub/org.freedesktop.LinuxAudio.Plugins.swh) -> La versión 21.08 si me funcionan
        * Para que reconozca los plugins tiene que coindicir la rama/branch de la app + runtime de la app con rama del plugin. Si la app usa el runtime 21.08 no detecta los 20.08 ejemplo.
    * OJO si ya tenemos instalado Audacity por repositorios con la configuración
        * Usa `~/.var/app/org.audacityteam.Audacity/` pero testimonialmente.
        * `~/.audacity-data` es donde realmente está lo importante y se mezlcan/rompen las configs al compartir los 2 Audacitys la misma carpeta.

## Grabación
* Edit -> Preferences -> Quality -> Default Sample Rate : 44100 Hz
    * Comprobar compatibilidad de dispositivos/micros
* Que el nivel de los picos de tu audio al grabar quede por debajo de -6 dB. -9 es buena cifra.

## Edición
https://manual.audacityteam.org/man/keyboard_shortcut_reference.html
* Atajos
    * Ctrl+A -> Seleccionar todo
    * Ctrl+B -> Etiquetar selección
    * Ctrl+L -> Silenciar selección
    * Ctrl+R -> Repeat Noise Reduction
* Configuración
    * Trabajar a 32bit float para no perder información

### Efectos
Orden a seguir:
1. Ecualizar > Ampliar graves
1. Ecualizar > Ampliar agudos (trebble)
2. Limpiar sonido / [Puerta de ruido]
3. Comprimir
4. [Deesser]
5. Normalizar (LUFS)/ Limitar

#### Reducción de Ruido
https://manual.audacityteam.org/man/noise_reduction.html

* Paso 1
    * Seleccionamos al menos 2 segundos (mejor 10) del audio que esten en silencio y se escuche el ruido
    * "Get Noise Profile"
    * "Preview" -> Para ver como quedaría
* Paso 2
    * Seleccionamos todo el audio al que queramos aplicar la limpieza de ruido
    * Valores por defecto:
        * Noise reduction (dB) : 12
        * Sensitivity : 6.00
        * Frequiency smoothing (bands) : 3
        * Noise : Reduce
    * Probar tambien :
        * NR 20 - S 04 - FS 03 : Markolino
        * NR 08 - S 10 - FS 0/1 <https://ana.mareca.es/como-grabar-un-podcast-en-audacity/>
* Repetir si hay varios tipos de ruidos

#### Ecualizar

* Para podcast [1](https://www.theseasonedpodcaster.com/audio-production-theory/best-equalizer-settings-for-podcasts-our-guide-to-voice-eq/) [2](https://www.glow.fm/guides-tutorials/how-to-make-your-podcast-sound-better-with-simple-eq-techniques) [3](https://streamgeeks.us/how-to-eq-a-podcast/)[4](https://www.youtube.com/watch?v=cvnMnwLfrNw)
    * 0-80 : a suelo en curva o diagonal (-30dB) (Low roll off for speech)
    * 200/250 : +3
    * 400 : -2
    * 500 : -3
    * 600 : -2
    * 700 : -1
    * 800-1000 : 0
    * 1100-17000 : +1
    * 19000-.. : a suelo

#### Noise Gate
* https://manual.audacityteam.org/man/noise_gate.html
    * Por defecto:
        Umbral: -40 - FR:0 - Reducción: -24 - A:10 - Mantener:50 - Decaimiento:100
* Markolino
    * FR: 0 - GT:-30 - LR:-20 - Attack/Decay:140 
    * FR: 0 - GT:-50 - LR:-100 - Attack/Decay:30

#### Compresión
https://manual.audacityteam.org/man/compressor.html

* Configuración por defecto (podcasting):
  * Curva de compresión
    * Threshold (dB) : -15 dB (A partir de cuando comprime)
         * 5-7dB por debajo de tu pico
    * Make-up gain (dB) : 1 (Amplify de todas las pistas para compensar)
    * Knee width (dB) : 24 (Como de suave o recto es el compresor)
    * Ratio : 3:1 (Cuanto comprimir una vez que alcanzamos el Threshold)
  * Suavizado
    * Lookahead (ms) : 1 (Comprime tanto antes de alzacanzar el T)
    * Attack (ms) : 15  (Comprime tanto despues de alzacanzar el T)
    * Release (ms) : 40 (Deja de comprimir tato despues tras salir del T)
* Probar: T -10 <-> -16 / Kw 9 / C 2:1 / A 30-50 / R 80-150

#### Normalización
https://manual.audacityteam.org/man/amplify_and_normalize.html

* Valores por defecto:
    * Remove DC offset (center on 0.0 vertically) : SI
    * Normalize peak amplitude to : -1.0 dB
    * Normalize stereo channels independently: NO
* No utilizar, mejor el siguiente con LUFS

#### Loudness Normalization
https://manual.audacityteam.org/man/loudness_normalization.html

* Valor por defecto : -23
    * Podcast
        * Estereo : -16 LUFS [1](https://podnews.net/article/lufs-lkfs-for-podcasters), [2](https://auphonic.com/help/algorithms/singletrack.html#global-loudness-normalization-and-true-peak-limiter)
        * Mono : -19 LUFS o dual_mono, pero no es el [estandar](https://auphonic.com/blog/2020/06/09/loudness-normalization-mono-productions/)

* Con [ffmpeg](/mundolibre/docs/multimedia/ffmpeg/#normalización) loudnorm
    * Por defecto: I=-24:LRA=7:TP=-2:linear=true::dual_mono=false
    * Para que se aplique el `linear`:
        * Añadir los parametros con las medidas de la primera pasada
        * Que el LRA final no sea menor que el inicial -> Esto suele hacer que sea casi siempre `dynamic`, max 50.
        * Que el TP no se pase
    * [Script](https://gitlab.com/Marzal/dmc-scripts/-/tree/master#dmc_fflufssh) casero [simplificado](https://gitlab.com/Marzal/hl-scripts/-/raw/master/bin/dmc_fflufs.sh?inline=false), equivalente a usar [ffmpeg-normalize](https://github.com/slhck/ffmpeg-normalize)
* Docu
    * [Webinar sobre normalización y sonoridad - Markolino Style](https://www.youtube.com/watch?v=HHkWU6AdRf4)
    * https://peterforgacs.github.io/2018/05/20/Audio-normalization-with-ffmpeg/
    * [Google Assistan](https://developers.google.com/assistant/tools/audio-loudness) I=-16:LRA=11:TP=-1.5:linear=true
    * [Amazon Alexa](https://developer.amazon.com/en-US/docs/alexa/flashbriefing/normalizing-the-loudness-of-audio-content.html) I=-14:LRA=11:TP=-3
    * https://www.reddit.com/r/AnimeThemes/wiki/encoding/ffmpeg/audio_normalization


#### Truncate Silence
https://manual.audacityteam.org/man/truncate_silence.html
* Por defecto
    * Level/Umbral: -20dB - Duration: 0,5s - Truncar 0,5s - [Comprimir: 50%] (Truncar silencio detectado)

* Probar:
    * Level/Umbral: -40dB - Duration: 0,4s - [Truncate 0,4s] - Comprimir: 50% (Compress Excess Silence)

## Exportar
https://manual.audacityteam.org/man/exporting_audio.html

### FLAC
https://manual.audacityteam.org/man/flac_export_options.html


### OGG
* Quality : 8 (Por defecto 5)

## Documentacion
* Markolino
    * [PodNoCostoso - #19. Procesando el podcast de la asocciación GNU-Linux Valencia - Parte 1 de 3](https://devtube.dev-wiki.de/videos/watch/93db0e5a-a113-4675-8a0e-3495481b6d58)
        * [Parte 2 de 3](https://devtube.dev-wiki.de/videos/watch/3575a888-fa1d-4c0a-a25b-46d6ef18e24b)
* Juan Febles
    * https://archive.org/details/charlaaudacity20
    * https://archive.org/details/CursoPodcasting2Audacity
    * https://liberaturadio.org/audacity-editar-voces-podcast/#1-ecualizar-las-voces