---
title: "Servidores de audio"
icon: fa-solid fa-audio-description
linkTitle: "Audio"
weight: 1
date: 2020-12-05
description: >
    ALSA, Pulse y Pipewire
categories: [Multimedia]
tags: [audio]
---

https://wiki.archlinux.org/index.php/Sound_system
## ALSA
* https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture
* No necesita instalación ya que es un subsistema del kernel

### Mostrar info
* Dispositivos
    ```bash
    ll /proc/asound
    grep . /proc/asound/{cards,devices,pcm}
    grep -R . /proc/asound/card1
    ```
    * Manera alternativa de mirarlo : aplay -l
    * ALSA Information Script
        * https://www.alsa-project.org/main/index.php/Help_To_Debug
## Pipewire
Servidor de sonido y video

### Paquetes
* Instalados
    * [pipewire](https://www.archlinux.org/packages/extra/x86_64/pipewire/) : Server and user space API to deal with multimedia pipelines
* Opcionales
    * [pipewire-alsa](https://www.archlinux.org/packages/extra/x86_64/pipewire-alsa/) : ALSA Configuration for PipeWire
    * [pipewire-jack](https://www.archlinux.org/packages/extra/x86_64/pipewire-jack/) : Server and user space API to deal with multimedia pipelines (JACK support)
        * [Wiki](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/JACK)
    * [pipewire-pulse](https://www.archlinux.org/packages/extra/x86_64/pipewire-pulse/) : Server and user space API to deal with multimedia pipelines (PulseAudio support)
        * OJO! que este en la versión 0.3.17 sustituye a pulseaudio y pulseaudio-bluetooth por completo
    * [wireplumber](https://archlinux.org/packages/extra/x86_64/wireplumber/)

### Mostrar config
```bash
systemctl --user --full --no-pager status pipewire-pulse.{socket,service} pipewire.service
pw-mon  # Monitor objects on the PipeWire instance
pw-cli ls # alias de list-objects, admite filtros: [all|Core|Module|Device|Node|Port|Factory|Client|Link|Session|Endpoint|EndpointStream|<id>]
```

### WirePlumber
Session manager para Pipweire que remplanza a `pipewire-media-session`

```bash
wpctl status # Mostrar objetos
```

## Pulseaudio
* https://wiki.archlinux.org/index.php/PulseAudio
* Servidor de sonido entre ALSA y las aplicaciones. Ahora mismo es el estandar de facto.

### Paquetes
* Instalados
    * pulseaudio : A featureful, general-purpose sound server
    * plasma-pa : Plasma applet for audio volume management using PulseAudio
        * libcanberra-pulse : PulseAudio plugin for libcanberra
* Opcionales
    * pulseaudio-alsa : ALSA Configuration for PulseAudio
    * pulseaudio-jack : Jack support for PulseAudio / for JACK sink, source and jackdbus detection
    * pavucontrol-qt : A Pulseaudio mixer in Qt (port of pavucontrol)

### Mostrar config
<https://forum.manjaro.org/t/sound-not-working-after-upgrade/2864/37>
* Estado de los servicios
```bash
systemctl --user --full --no-pager status pulseaudio.{socket,service} 
```
* Comprobar los modulos
```bash
pactl list short modules
```
* Mostrar configuración
```bash
pactl stat # Paquete libpulse
pactl info
pacmd dump # Paquete pulseaudio
```
* Listar
```bash
pacmd list
pacmd list-cards
pacmd list-sinks
pacmd list-sink-inputs
pacmd list-sources
```

### Acciones
* Reiniciar
```bash
systemctl --user restart pulseaudio
```

## Programas
* https://gitlab.com/nodiscc/awesome-linuxaudio#awesome-linuxaudio

## Docu
* https://wiki.thingsandstuff.org/Audio