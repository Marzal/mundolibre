---
title: "Servidor de sonido JACK"
icon: fa-solid fa-file-audio
linkTitle: "JACK"
weight: 99
date: 2020-11-15
description: >
    Para profesionales del audio. Aunque lo nuevo es Pipewire
categories: [Multimedia]
tags: [audio]
---
[Jack2](https://archlinux.org/packages/community/x86_64/jack2/) es el que está en desarrollo, jack1 está en bugfix only.

## Instalación
```bash
pacman -S jack2 carla # Basico
pacman -S pulseaudio-jack jack_mixer # Recomendable
pacman -S cadence || yay -S studio-controls-git # Solo se puede tener uno
pacman -S libffado # Opcionales
# qjackctl # -> no recomendado por pulseaudio bridge [8]
# calf meterbridge zam-plugins x42-plugins tap-plugins mcp-plugins lsp-plugins fil-plugins dpf-plugins cmt caps amb-plugins zynaddsubfx # Plugins
```

## Permisos
* Consideraciones sobre permisos <https://wiki.archlinux.org/index.php/Users_and_groups#Pre-systemd_groups>

### Semi auto con el paquete [realtime-privileges](https://www.archlinux.org/packages/community/any/realtime-privileges/)

```bash
pacman -S realtime-privileges

groups $USUARIO | grep realtime || usermod -aG realtime $USUARIO
# Cadence comprueba si estamos en el grupo audio, pero no tiene por que ser ese grupo:
# https://wiki.linuxaudio.org/wiki/cadence_introduction
```

### Manual con el grupo existente audio
/etc/security/limits.d/audio.conf
```bash
@audio   -  rtprio     95
@audio   -  memlock    unlimited
```
```bash
groups $USUARIO | grep audio || usermod -aG audio $USUARIO
```

## Info
* Arranque
    * /usr/share/dbus-1/services/org.jackaudio.service -> `/usr/bin/jackdbus auto`
    * ~/.log/jack/jackdbus.log

## Comprobaciones
```bash
fuser /dev/snd/*
ps ax | grep [PID here]
ps -ef | grep -iE 'cadence|studio|jack|pulse'
journalctl -b | grep -iE 'pulse|jack|alsa'

ulimit -r -l
lsof +c 0 /dev/snd/pcm* /dev/dsp* || fuser -fv /dev/snd/pcm* /dev/dsp*
```

## Ficheros de configuraciones
* [jackdrc](https://github.com/jackaudio/jackaudio.github.com/wiki/jackdrc(5))

* Listar
```bash
find /home/*/.log -type f -ls
find /home/*/.config/ -type f -iname "*jack*" -ls
# ALSA settings
ls -l ~/.asoundrc
# Pulse settings
ls -l ~/.pulse
# JACK settings
ls -l ~/.config/jack
# Cadence settings
ls -l ~/.config/Cadence
# Studio-Controls settings
ls -l ~/.config/autojack/
```
* Borrar / Limpiar
```bash
rm -fr ~/.asoundrc ~/.pulse ~/.config/jack/conf.xml ~/.config/Cadence/* ~/.jackdrc ~/.config/rncbc.org/QjackCtl.conf
```
### ~/.config/jack/conf.xml
```bash
<jack>
 <engine>
  <option name="driver">alsa</option>
 </engine>
 <drivers>
  <driver name="alsa">
   <option name="device">hw:S,0,0</option>
   <option name="capture">hw:S,0</option>
   <option name="playback">hw:S,0</option>
   <option name="period">1024</option>
   <option name="inchannels">1</option>
   <option name="outchannels">2</option>
  </driver>
 </drivers>
</jack>
```

## Configuración via utilidades

### jack_control
* Se instala con [jack2-dbus](https://archlinux.org/packages/community/x86_64/jack2-dbus/)
```bash
jack_control status
jack_control dl     # get list of available drivers
jack_control dg     # get currently selected driver
jack_control dp     # get parameters of currently selected driver
jack_control il     # get list of available internals
jack_control ep     # get engine parameters

jack_control ds alsa
jack_control dps device hw:S,0,0 dps rate 48000 # No funciona, solo un comando por ejecucion
jack_control dps period 1024    # Así
jack_control dps nperiods 2     # si
```
### Cadence
* Info
    * NO detecta automaticamente las entradas y salidas, pero se pueden poner manualmente.
* Config
    * JACK Bridges:
        * ALSA Audio -> Bridge Type : ALSA -> PulseAudio -> JACK (Plugin) [8]
        * PulseAudio -> Auto-start at login = `pulseaudio --daemonize --high-priority --realtime --exit-idle-time=-1 --file=/usr/share/cadence/pulse2jack/play+rec.pa -n`
* Ficheros
    * ~/.asoundrc
    * ~/.config/Cadence/JackSettings.conf
* Arranque
    * /etc/xdg/autostart/[cadence-session-start.desktop](https://github.com/falkTX/Cadence/blob/master/data/autostart/cadence-session-start.desktop) -> `/usr/bin/cadence-session-start --maybe-system-start` [2](https://github.com/falkTX/Cadence/blob/master/data/cadence-session-start) -> `/usr/share/cadence/src/cadence_session_start.py --system-start-desktop` [3](https://github.com/falkTX/Cadence/blob/master/src/cadence_session_start.py)
    * /etc/X11/xinit/xinitrc.d/61cadence-session-inject -> `/usr/bin/cadence-session-start --system-start-by-x11-startup $STARTUP` -> `/usr/share/cadence/src/cadence_session_start.py --system-start`
        * Llamado por /usr/share/sddm/scripts/Xsession
* Inhabilitar
    * `echo 'Hidden=true'> ~/.config/autostart/cadence-session-start.desktop`

#### Comprobaciones
```bash
journalctl -b | grep cadence
ps -ef | grep cadence
```

### Studio Controls
https://github.com/ovenwerks/studio-controls/wiki
* Info
    * Detecta automaticamente las entradas y salidas, pero no deja configurarlas manualmente
    * Permite modificar el nombre de los puentes de Pulse para que aparezcan en Carla facilmente identificables
* Dependencias
```bash
pacman -S python-dbus python-jack-client zita-ajbridge a2jmidid
# opcionales python-numpy alsa-utils
yay -S python-pyalsaaudio
```
* Arranque:
    * studio.service -> /usr/bin/autojack

#### Comprobaciones
```bash
systemctl --user status studio.service
systemctl --user status session-monitor.service

sudo systemctl status studio-system.service

tail -qn 60 ~/.log/jack/jackdbus.log ~/.log/autojack.log

find /home/*/.asoundrc -exec echo '==> {} made by Cadence may not be compatible with Studio-Controls' \; 2>/dev/null
pacman -Ql python-pyalsaaudio | grep 3.8 && echo "==> Please reinstall python-pyalsaaudio cleaning the cache, it shouldn't be found in python3.8 directory"
```

#### Config Jack Master Setting
* Kingston HyperX Cloud Revolver S
    * 48000 - 32 - 3 : No va
    * 48000 - 64/128 - 3 : Xruns
    * 48000 - 256 - 3 : 

#### Config HW de Extra Devices
* Radeon RX580
    * HDMI,11,0 playback (HDMI 5) -> DP1 Central : 1 puerto
    * HDMI,10,0 playback (HDMI 4) -> DP0 Lateral : 2 puertos
    * HDMI,9-7,0 playback (HDMI 3-0) -> Sin conexion : 1 puerto    

### pactl
* Info
    * [Modulos pulse](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-jack-sink)
```bash
pactl load-module module-jack-sink client_name=NOMBRE_OUTput sink_properties=device.description='"JACK\ Sink\ (client_name)"'
pacmd list-sinks
```

## Manejo

### Session Manager 
Sirve para guardar configuraciones de parcheo y estados y poder tener diferentes sesiones/configuraciones fácil de volver a cargar sin hacerlo todo de nuevo.

* Non-Session-Manager es el original, que tiene un fork mas moderno [New-Session-Manager](https://linuxaudio.github.io/new-session-manager/) que es ahora el recomenado y se puede usar con GUIs como [Agordejo](https://www.laborejo.org/agordejo/)

* [NSM](https://archlinux.org/packages/community/x86_64/new-session-manager/) -> [Agordejo](https://archlinux.org/packages/community/x86_64/agordejo/)
    * [Historia](https://jackaudio.org/news/2020/07/16/jack-session-has-been-marked-as-deprecated.html)

### Carla
* https://kx.studio/Applications:Carla
* Plugin host + Patchbay.
* Configure Carla :
    * Main:
        * Enable experimental features: Si
    * Experimental:
        * Enable plugin bridges: Si
        * Enable jack applications: Si
    * Engine:
        * Audio driver: JACK
        * Process mode: Single Client

### Catia
* Pathbay incluido en el paquete cadence. (Menos potente que Carla)

## Aplicaciones Clientes
### [Firefox](https://archlinux.org/packages/extra/x86_64/firefox/)
* Necesita [FLAG](https://bugzilla.mozilla.org/show_bug.cgi?id=783733#c81) en compilación. [Arch](https://github.com/archlinux/svntogit-packages/blob/packages/firefox/trunk/PKGBUILD#L92) la tiene.
    * Pero si hay pulseaudio disponible no la usa por prioridad.

## Latency

```bash
jack_control dps input-latency 17
jack_control dps output-latency
```
* https://wiki.linuxaudio.org/wiki/list_of_jack_frame_period_settings_ideal_for_usb_interface
* https://open.tube/videos/watch/128b9287-9303-41d2-8e33-70a3b56b0227
* https://linuxmusicians.com/viewtopic.php?f=19&t=8022
* https://kx.studio/Documentation:Manual:latency

## Documentacón
* https://wiki.archlinux.org/index.php/Professional_audio
    * https://wiki.archlinux.org/index.php/JACK_Audio_Connection_Kit
    * [8](https://wiki.archlinux.org/index.php/PulseAudio/Examples#PulseAudio_through_JACK)
* https://salmorejogeek.com/2019/08/31/instalar-y-configurar-jack-y-pulseaudio-bridge-via-cadence-en-arch-linux-y-basadas-extra-ardour/
    * [https://www.youtube.com/watch?v=3DLdLaNT84I&list=PLYfAb9IwgNzoOc76iOhpJMvy-itR0uL1_](Lista de reproduccion de JACK)
    * https://www.youtube.com/watch?v=TfekotaztZM - Mi estado actual en Youtube y Arch Linux con JACK, Cadence y Carla (Octubre 2020)
    * https://www.youtube.com/watch?v=ffA0ReQXBZI - Empezando con Carla
* https://www.youtube.com/watch?v=Ga3cHDkA18c - Ardour y Jack... Por partes #06 | Procesamiento de voz con Carla
* https://www.youtube.com/watch?v=6vhfIBvAQAQ - Carla con conexiones persistentes en aplicaciones

* https://facilitarelsoftwarelibre.blogspot.com/2018/05/instalar-y-configurar-jack-audio.html
* https://colaboratorio.net/xphnx/multimedia/audio/2017/optimizar-equipo-jack-configuracion-cadence/
