---
title: "Herramientas con algo de IA"
icon: fa-solid fa-robot
linkTitle: "IA self-hosted"
weight: 99
date: 2024-03-09
description: >
  Algunas herramientas de las llamadas "IA" pero autoalojadas y usables desde CPU
categories: [Aplicaciones]
tags: [IA,self-hosted]
---

## Whisper.cpp
Transcripción de audio. Audio a texto.
* https://github.com/ggerganov/whisper.cpp
```bash
INPUT=audio.ogg
ffmpeg -i "$INPUT" -ar 16000 -ac 1 -c:a pcm_s16le "${INPUT%.*}.wav"     # Tiene que ser WAV 16
whisper.cpp-large-v3-q5 -t 6 -ovtt -osrt -otxt -oj -l es -sow -ml 32 -f "${INPUT%.*}.wav" -of "${INPUT%.*}"
# Si se atasca mucho y hay repeticiones añadir : -mc 64 -et 2.8
# https://github.com/ggerganov/whisper.cpp/issues/896#issuecomment-1569586018
```
* Transformar de JSON a SRT : https://github.com/dev-vinc/whisper-json-pharser

KDE Express 27 de 23m30s de duración:
| Modelo  (16GB RAM)   |id| GB  | Tiempo | Render | Comentario
| -------------------- |--| --- | -------| ------------- | ------
|whisper:large-v3-q5 ⭐️|  | 1.1 | 27m21s | i5-8400 + rusticl RX580 | No se cuelga y buen resultado
|whisper:large-v2    ✅|  | 2.9 | 32m32s | i5-8400 + rusticl RX580 |
|large-v3 -mc 64 -et 2.8|  | 2.9 | 29m49s | i5-8400 + rusticl RX580 | No se cuelga pero empeora un poco
|whisper:medium        |  | 1.5 |  | i5-8400 + rusticl RX580 |  
|whisper:large-v3    🤯|  | 2.9 | 1h01m | i5-8400 + rusticl RX580 | Se queda colgado repitiendo frases 


## ollama
Utillidad para lanzar localmente diferentes LLM facilmente con descarga de modelos automática.
* https://ollama.com/
```bash
ollama serve
```

### LlaVa
Reconocimiento de imagenes con funcion de OCR. Imagen a texto
* https://github.com/haotian-liu/LLaVA

```bash
ollama run llava:13b-v1.6-vicuna-q4_0 "Describe en detalle esta imagen por favor. /logo-ATL-opt.png"
```
| Modelo     (16GB RAM PC)    |    id        | GB  | Tiempo    | Equivalencias | Render
| --------------------------  | ------------ | --- | --------- | ------------- | ------
|llava:34b-v1.6-q4_0          | 3d2d24f46674 | 20  | **2h20m -SDD**| 34b 34b-v1.6 34b-v1.6-q4_0 | i5-8400
|llava:13b-v1.6-vicuna-q8_0   | b25c4195f9e2 | 14  | **35m-1h-SDD**| - | i5-8400
|llava:13b-v1.6-vicuna-q5_K_M | 446b10458a6a | 9.9 | 8--**35m**    | - | i5-8400
|llava:13b-v1.6-vicuna-q4_K_M | 0843119c3874 | 8.5 | 4-7-27m     | - | i5-8400
|llava:13b-v1.6-vicuna-q4_1   | 0b97528b26b0 | 8.8 |           | - | i5-8400
|llava:13b-v1.6-vicuna-q4_0 ⭐️| 0d0eb4d7f485 | 8.0 | 4-7-16m +MT  | 13b 13b-v1.6 13b-v1.6-vicuna-q4_0 | i5-8400
|llava:7b-v1.6-mistral-q8_0   | c2973e390e84 | 8.3 | 3-5m +MT  | - | i5-8400
|llava:7b-v1.6-mistral-q6_K   | 8781169d7f8f | 6.6 | 3-5m +MT  | - | i5-8400
|llava:7b-v1.6-mistral-q4_0 🤯| 8dd30f6b0cb1 | 4.7 | 2-4m +MT  | latest 7b v1.6 7b-v1.6 7b-v1.6-mistral-q4_0 | i5-8400

[Si aparece 576 tokens o menos es 1.5](https://github.com/ggerganov/llama.cpp/blob/master/examples/llava/README.md#how-to-know-if-you-are-running-in-llava-15-or-llava-16-mode):
```
encode_image_with_clip: image embedding created: 576 tokens
```

Pruebas:
1. ¿Qué aparece en esta imagen? Responde en castellano o español por favor. /home/dmc/logo-ATL-opt.png
2. Describela en detalle por favor.

* **llava:13b-v1.6-vicuna-q4_0** ⭐️
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
Por ahora el mejor en relación calidad / tiempo.
  {{% /tab %}}  
  {{% tab header="1:04min⭐️" lang=es text=true %}}
La imagen muestra un logotipo que incluye el texto "ACCESSIBILIDAD CON TECNOLOGIAS LIBRES". A la derecha del logotipo, hay símbolos representativos de accesibilidad, incluyendo una silla de ruedas, una persona con un bastón y una persona con discapacidades visuales que parece estar guiada por otra persona. Estos elementos simbolizan la inclusión y el acceso para personas con diferentes necesidades en cuanto a la tecnología y la accesibilidad.
  {{% /tab %}}
  {{% tab header="2:7min⭐️" lang="markdown" text=true %}}
La imagen es un logotipo o emblemo que está diseñado para resaltar la importancia de la accesibilidad en tecnología libre.
En el centro del logotipo, se encuentra el texto "ACCESSIBILIDAD CON TECNOLOGIAS LIBRES" escrito en una fuente legible y en un color contrastante, lo que facilita su lectura para personas con diversas necesidades visuales.

A la derecha del logotipo hay tres símbolos representativos de la accesibilidad:
- Un símbolo que indica la presencia de un usuario de silla de ruedas, que es comúnmente utilizado para significar accesibilidad para personas con movilidad reducida.
- Un símbolo que muestra una persona ~~con una cana~~, lo cual también se utiliza como un símbolo universal de accesibilidad para personas con discapacidades visuales o auditivas.
- Un símbolo que representa la guía a través del uso de un bastón, que es utilizado para ayudar a las personas con discapacidades visuales a moverse por espacios públicos.

Todos estos elementos están dispuestos en una línea horizontal, con un fondo neutro y bordes gruesos para facilitar su visibilidad y legibilidad. La imagen se utiliza probablemente como un logotipo o emblemo que promueve la inclusión y el acceso para personas con diversas necesidades a través de tecnologías libres, es decir, aquellas que no están sujetas a derechos de propiedad intelectual.
  {{% /tab %}}
{{< /tabpane >}}

* llava:13b-v1.6-vicuna-q4_K_M
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
Más lento que llava:13b-v1.6-vicuna-q4_0 y alucina.
  {{% /tab %}}  
  {{% tab header="1:4min✅" lang=es text=true %}}
La imagen muestra una logotipo con el texto "ACCESSIBILIDAD CON ~~TECHNOLOGÍAS~~ LIBRES" en español. A continuación, hay iconos representando una persona silla de ruedas, un par de manos con la señal de que no están usando algo (quizás sordo o ciego), y una persona usando un bastón. Estos elementos suelen ser asociados con la inclusión y accesibilidad para personas con discapacidades, lo que se refleja en el contexto del mensaje. El logotipo parece estar relacionado con una organización o proyecto que promueve la accesibilidad en tecnologías de manera libre y gratuita.

Tirando de disco:
La imagen muestra ~~una página web o un sitio de internet~~ con el título "Accesibilidad con tecnologías ~~liberes~~". ~~Hay un enfoque artístico en la parte superior izquierda, que parece ser la mano y el brazo de una persona en movimiento.~~ A continuación se muestran tres símbolos o logotipos: el primer uno es una ~~caja con una rueda de silla~~, lo segundo muestra un personaje en una silla de ruedas y el tercero está formado por ~~tres líneas verticales~~, que podrían simbolizar accesibilidad. El diseño sugiere temáticas relacionadas con la inclusión y la adaptabilidad para personas con discapacidades.
  {{% /tab %}}
  {{% tab header="2:7-9m✅" lang="markdown" text=true %}}
 La imagen es un logo o símbolo que promueve la "ACCESSIBILIDAD CON TECNOLOGÍAS LIBRES". El diseño central muestra ~~una rueda de caracol~~, que está asociada con la accesibilidad para personas con discapacidades visuales. Alrededor de la ~~rueda de caracol~~ se encuentran símbolos y figuras universales relacionadas con la inclusión y el accesible:

- En la parte superior izquierda, ~~un tridente~~, que puede representar una silla de ruedas o algún otro tipo de soporte.
- En la parte inferior izquierda, una figura en movimiento con una guía, lo que sugiere orientación o guía para personas con discapacidades visibles.
- En la parte superior derecha, un símbolo de un personaje sentado en un asiento de ruedas, que es comúnmente utilizado para representar accesibilidad para personas en sillas de ruedas.

El color principal del logo es rojo y se utiliza para destacar el mensaje central sobre la importancia de la accesibilidad tecnológica para todos. La imagen tiene un aspecto moderno y profesional, lo que sugiere que la organización o iniciativa que promueve este logotipo está comprometida con la inclusión y la igualdad de oportunidades en el ámbito digital.
  {{% /tab %}}
{{< /tabpane >}}

* llava:13b-v1.6-vicuna-q5_K_M
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
Más lento que llava:13b-v1.6-vicuna-q4_0 y no genera mejores resultados. La versión detallada es buena, pero mejor.
  {{% /tab %}}  
  {{% tab header="1:18min" lang=es text=true %}}
La imagen muestra un logotipo que representa "Accessibilidad con tecnologías ~~liberadas~~". En la parte inferior de la imagen
hay símbolos icónicos que suelen asociarse con accesibilidad en informática y tecnología, como una silla de ruedas, una 
persona con ~~muletas~~ y un ~~guante abierto~~. Estos símbolos son comunes para representar accesibilidad en la web y la
tecnología para usuarios con diferentes necesidades y discapacidades.
  {{% /tab %}}
  {{% tab header="2:8min✅" lang="markdown" text=true %}}
La imagen es una infografía con un tema relacionado a la inclusión y accesibilidad en tecnologías de la información. En la
parte superior de la imagen, se lee "ACCESSIBILIDAD CON TECNOLOGÍAS LIBRES" en letras grandes y negras sobre un fondo 
blanco. A continuación, se muestran tres símbolos universales para la accesibilidad, cada uno representando una persona con
necesidades diferentes:

1. Un hombre en silla de ruedas, simbolizando la accesibilidad para personas con movilidad limitada.
2. Un signo manual de "OK" o "correcto", que podría indicar la comprensión y la inclusión.
3. Un ~~perro guía~~, lo cual representa a los canes guías utilizados por personas ciegas o con discapacidad visual.

Los símbolos están coloridos (~~verde~~, azul y rojo) y se superponen entre sí en un diseño circular. En el centro de la 
imagen, se encuentra una representación stilizada de una rueda de engranajes o mecanismo, sugiriendo interconexión o 
integración de tecnologías y accesibilidad. El fondo de la infografía es blanco con un patrón gráfico que parece ser una 
representación abstracta de las garras o manos.

El diseño es moderno, utilizando una combinación de símbolos universales para la inclusión y un mecanismo central que 
simboliza la tecnología y la interconexión entre los componentes para ilustrar el tema de accesibilidad con tecnologías 
libres.
  {{% /tab %}}
{{< /tabpane >}}


* **llava:13b-v1.6-vicuna-q8_0**
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
Tasa lectura disco 1.7 GiB/s Plasma  840 M/s htop (cuello de botella?)
  {{% /tab %}}
  {{% tab header="1:35m✅" lang=es text=true %}}
La imagen muestra una banner con el texto "ACCESSIBILIDAD CON ~~TECHNOLOGÍAS~~ LIBRES" en español. Además, hay símbolos de accesibilidad y tecnología que representan la inclusión y el apoyo para personas con discapacidades. El diseño utiliza un colorido vibrante y es utilizado para promover la inclusión digital a través de soluciones tecnológicas que sean gratuitas o de código abierto, lo cual facilita la accesibilidad en el contexto de la tecnología digital.
  {{% /tab %}}
  {{% tab header="2:1h✅" lang="markdown" text=true %}}
La imagen es un banner promocional que trata sobre la accesibilidad con tecnologías libres. El diseño está enfocado en la inclusión y el apoyo para personas con discapacidades, utilizando símbolos universales de accesibilidad como un icono de una persona en silla de ruedas, un icono de un ~~guía canino~~ y un ícono de una mano ~~con un corazón~~. El color principal es rojo, pero también se incluyen otros colores como azul, ~~verde~~ y amarillo para resaltar la importancia del tema. El texto enespañol se encuentra encima del diseño y está enfocado en destacar el compromiso de proporcionar tecnologías libres para fomentar la inclusión. Este tipo de banner podría estar asociado con organizaciones o proyectos que trabajan en promover la
accesibilidad digital a través de soluciones tecnológicas gratuitas y de código abierto.
  {{% /tab %}}
{{< /tabpane >}}


* ***llava:7b-v1.6-mistral-q8_0***
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
* ✅ [¿Qué aparece en esta imagen? Responde en castellano o español por favor] Escueto pero salida OK con ATL
  * 🤯 [Describe detalladamente todo lo que aparece en esta imagen en español por favor.] + detallada pero alucina
* 🤯 Alucina con el logo de Fediverse
  {{% /tab %}}
  {{% tab header="1:03min✅" lang=es text=true %}}
En esta imagen, se ve un texto que dice ~~"ACCESSIBLE TECHNOLOGIES"~~. También hay una representación gráfica con tres iconos que simbolizan tecnología adaptable para personas con discapacidad:
1. Una iconografía de una persona en una silla de ruedas, sugiéndole la facilidad para moverse.
2. Un icono que representa un guía visuel, indicando que es fácil tener asistencia a personas con discapacidad visual.
3. Un icono que muestra una persona andando con ayuda de un bastón, sugiéndole la facilidad para moverse para personas con discapacidad motriz.
  {{% /tab %}}
  {{% tab header="2:05min🤯" lang="markdown" text=true %}}
La imagen es un cartel o póster que promueve la accessibilidad de tecnologías. En él, se presenta un texto en castellano o español que dice ~~"ACCESSIBLE TECHNOLOGIES"~~. A continuación, hay una representación gráfica con tres iconos que simbolizan tecnología adaptable para personas con discapacidad:
1. Una iconografía de una persona en una silla de ruedas, sugiéndole la facilidad para moverse. Esta icona incluye un elemento adicional que parece ser una mano sosteniendo a la persona en la silla de ruedas, lo que podría indicar asistencias adicionales o accesorios.
2. Un icono que representa un guía visuel, indicando que es fácil tener asistencia a personas con discapacidad visual. ~~Este icono incluye una figura humana y un libro, lo que podría sugerir que la tecnología adaptable incluye recursos de aprendizaje y consulta.~~
3. Un icono que muestra una persona andando con ayuda de un bastón, sugiéndole la facilidad para moverse para personas con discapacidad motriz. ~~Esta icona también tiene un elemento adicional que parece ser una mano sosteniendo a la persona andando con el bastón, lo que podría indicar asistencias adicionales o accesorios.~~

El estilo de la imagen es gráfico y simbólico, utilizando colores vivos (rojo, ~~verde~~ y amarillo) para hacerla atractiva y fácil de comprender. El diseño incluye un fondo blanco que contrasta con los iconos y el texto, lo que hace que la información sea clara y fácilmente entendible.
  {{% /tab %}}
{{< /tabpane >}}

* ***llava:7b-v1.6-mistral-q6_K***
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
* 🤯 [Describe esta imagen detalladamente en español o castellano, por favor.] Alucina un poco con ATL.
* 🤯 [Describe detalladamente todo lo que aparece en esta imagen en español por favor.] Alucina con ATL
  {{% /tab %}}
  {{% tab header="1:3m30s" lang=es text=true %}}
En esta imagen, hay un logotipo que dice "Accessibilidad en Tecnología" y se encuentra dentro de una ilustración que muestra una representación estilizada de una persona en silla de ruedas. También hay otros iconos que simbolizan diversas facetas de la accesibilidad, como la libertad de movimiento, la inclusión y el respeto a los derechos humanos. La imagen parece ser un cartel o un póster sobre temas relacionados con tecnología y accesibilidad para personas con discapacidad.
  {{% /tab %}}
  {{% tab header="2:4m24s🤯" lang="markdown" text=true %}}
La imagen es un cartel o póster promocional sobre accesibilidad en tecnología. En el centro de la imagen, se encuentra ~~una
palabra "Accessibilidad en Tecnología" en letras mayúsculas y negras~~, lo que indica que el contenido del cartel es 
relacionado con este tema específico.

A continuación, se muestran tres iconos diferentes que simbolizan la idea de accesibilidad:

1. Una representación estilizada de una persona en silla de ruedas, lo que sugiere la importancia del diseño inclusivo y la
adaptación para personas con discapacidades físicas.
2. ~~Iconos que simbolizan la libertad de movimiento~~, lo que podría incluir la idea de la autonomía y la capacidad de las 
personas con discapacidades para desplazarse por sí mismas.
3. ~~Iconos que representan diferentes aspectos de la inclusión~~, como la igualdad y el respeto a los derechos humanos, lo que
indica que la accesibilidad en tecnología implica una visión holística y comprensiva de las personas con discapacidades.

La imagen es un medio visual para sensibilizar sobre los beneficios y la importancia de incluir a las personas con 
discapacidades en el desarrollo y uso de tecnología.
  {{% /tab %}}
{{< /tabpane >}}

* **llava:7b-v1.6-mistral-q4_0**
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
Rápido y sobrado para MT. Pero impreciso.
  {{% /tab %}}
  {{% tab header="1:2m27s🤯" lang=es text=true %}}
La imagen muestra un banner con la frase "ACCESIBILIDAD Y TECNOLOGÍAS LIBRES" y una representación gráfica de la 
accesibilidad. ~~La palabra "ACCESSIBILITY" está en inglés, lo que indica que la imagen está en lengua inglesa. La representación gráfica incluye un símbolo que parece ser un gatito, un guión~~ y una representación de una persona con discapacidad utilizando un trípode para asistir. ~~Estos elementos simbolizan la facilidad de acceso a los espacios públicos para personas con discapacidades ~~y el uso de tecnologías adaptadas para mejorar la accesibilidad.
  {{% /tab %}}
  {{% tab header="2:3m33s🤯" lang="markdown" text=true %}}
La imagen presenta un banner que promueve la accesibilidad y los tecnologías libres. El banner ~~está en inglés, con la frase "ACCESSIBILITY AND FREE TECHNOLOGIES"~~ escrita en español, lo que indica que la accesibilidad es una característica importante para el uso de tecnología libre.

En la parte superior del banner, hay un símbolo gráfico que ~~parece ser un gatito, que simboliza la libertad y la inclusión. Justo debajo del gatito, hay un trípode, lo que significa la necesidad de una infraestructura sólida para que las personas con discapacidades puedan acceder a los espacios públicos.~~

En el centro del banner, hay una representación de una persona con discapacidad utilizando un trípode para asistirse. Esto simboliza la utilidad de tecnologías adaptadas para mejorar la accesibilidad y facilitar el acceso a los espacios públicos.

La imagen está diseñada para llamar la atención a las personas con discapacidades y a aquellas que buscan tecnología libre para mejorar su accesibilidad, como son los desarrolladores de software libres.
  {{% /tab %}}
{{< /tabpane >}}

* ***llava:34b-v1.6-q4_0***
{{< tabpane right=false persist=header >}}
  {{% tab header="**Pruebas**:" text=true %}}
8m empezar + **2h20m** para terminar - Tasa lectura disco 1.7 GiB/s Plasma / 840 M/s htop (cuello de botella?)
  {{% /tab %}}
  {{% tab header="1:2h20m🤯" lang=es text=true %}}
La imagen muestra un grupo de símbolos relacionados con la accesibilidad para personas con discapacidades. Hace referencia a diferentes aspectos que promueven la inclusión y el uso de tecnología libre. Los símbolos incluyen ~~un braille~~, una persona en silla de ruedas, ~~una escopeta de código (que simboliza la accesibilidad digital), una persona con un brazo amputado y una etiqueta de audificador~~. Estos elementos juntos reflejan la idea de que todos deben tener acceso a las tecnologías y servicios, independientemente de sus capacidades o limitaciones físicas.
  {{% /tab %}}
{{< /tabpane >}}


### Llama
```bash
ollama run llama2 "Resume este fichero: $(cat 24-KDE_Express-Eventos-Transcripcion.txt)"
```

| Modelo               |    id        | GB  | 16GB RAM | Equivalencias | Render
| -------------------- | ------------ | --- | -------- | ------------- | ------
|llama2:13b-chat-q4_0  | d475bf4c50bc | 7.4 | 12m +MT  | 13b 13b-chat 13b-chat-q4_0 | i5-8400
|llama2:7b-chat-q4_0 ⭐️| 78e26419b446 | 3.8 | 07m +MT  | latest 7b chat 7b-chat 7b-chat-q4_0 | i5-8400

## Llama.cpp
https://github.com/ggerganov/llama.cpp

Soporta muchos modelos.
* Llava: https://github.com/ggerganov/llama.cpp/blob/master/examples/llava/README.md
  * https://huggingface.co/cmp-nct/llava-1.6-gguf
* Llama2-chat: https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGUF
* Para convertir modelos de *.pth (necesita tokenizer.model) : https://github.com/ggerganov/llama.cpp/issues/493

## Stable Difusion
De texto a Imagen.
* WebUIs
  * Interfaz web sencilla: https://easydiffusion.github.io/
  * ¿Más potente? : https://github.com/AUTOMATIC1111/stable-diffusion-webui

Easy Difussion soporta SD 1.5 y SDXL
* Funcionan:
  * sd-v1-5.safetensors : 
  * v2-1_512-nonema-pruned.safetensors : [get](https://huggingface.co/stabilityai/stable-diffusion-2-1-base/) - 5m por imagen en un i5-8400
* Me cuelgan el ordenador al usarse con CPU:
  * sd_xl_base_1.0_0.9vae.safetensors [1](https://github.com/easydiffusion/easydiffusion/wiki/SDXL)
  * sd_xl_base_1.0.safetensors
  * v2-1_512-ema-pruned.safetensors
* Configuración : https://github.com/easydiffusion/easydiffusion/wiki/UI-Overview

### ControlNet 1.5
https://github.com/lllyasviel/ControlNet-v1-1-nightly

* 👍 control_v11p_sd15_canny : Respeta el original
* ⭐️ control_v11p_sd15_softedge : Respeta el original
* __Robustness__: SoftEdge_PIDI_safe > SoftEdge_HED_safe >> **SoftEdge_PIDI** > SoftEdge_HED
* __Maximum result quality__: SoftEdge_HED > **SoftEdge_PIDI** > SoftEdge_HED_safe > SoftEdge_PIDI_safe
* control_v11p_sd15_scribble : Hace mezclas
* control_v11e_sd15_shuffle : shuffle -> NO
* control_v11p_sd15_mlsd : mlsd -> Pasa de la imagen de ControlNet
* ~~control_v11p_sd15_seg~~ :  The module 'mediapipe' is not installed 🤷‍♂️
* control_v11f1p_sd15_depth :
  - normal BAE*
  - midas
  - zoe : Error 🤷‍♂️
  - leres* : Error 🤷‍♂️

## Documentación general

* Quantización, variantes de los modeles: https://towardsdatascience.com/quantize-llama-models-with-ggml-and-llama-cpp-3612dfbcc172
  * Recomienda Q5_K_M -> Q4_K_M
