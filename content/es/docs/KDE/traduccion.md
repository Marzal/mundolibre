---
title: "Traducción"
icon: fa-solid fa-language
linkTitle: "Traduccion"
weight: 1
date: 2021-11-26
description: >
    Guia básica de traducir para KDE
categories: [Traduccion]
tags: [Traduccion,KDE]
---

# General
## Pasos
1. Leerse al menos la documentación de https://es.l10n.kde.org
2. Apuntarse a la lista de distribución y comentar que se quiere traducir
3. (opcional) configurar KSvnUpdater y/o Lokalize
4. Repasar la traducción y el glosario
5. Enviarle la traducción al coordinador

* Cosas a tener en cuenta
    * Comillas dobles inglesas (””) -> las españolas («»).
    * Normalmente palabras que no están en la RAE van sin -, como ecoetiqueta.
    * Extranjerismos van en cursiva (*palabra*).
        * Pero hay que vigilar que sea en un sitio que soporte el formato.
    * SIEMPRE usar de usted.
    * Intentar no usar abreviaturas como «p. ej.» por «por ejemplo».
    * Usar computadora en vez de ordenador o equipo.

## KSvnUpdater

* Asistente
    * Coordinador: ecuadra .arroba. eloihr.net
    * Lista de distribución de su equipo: kde-l10n-es@kde.org
    * Glosario en línea de su equipo: https://es.l10n.kde.org/terms.tbx
    * Asignaciones en línea de su equipo: https://es.l10n.kde.org/asignaciones2.xml
        ** Útil para el coordinador

## Documentación útil
* https://es.l10n.kde.org
    * [Glosario](https://es.l10n.kde.org/glosario.php)
    * [Asignacion de trabajo](https://es.l10n.kde.org/asignaciones.php)
* [Localization](https://techbase.kde.org/Localization)
    * Lokalize [Web](https://userbase.kde.org/Lokalize) - [Manual](https://docs.kde.org/trunk5/en/lokalize/lokalize/index.html)
* [The KDE Translation HOWTO](https://l10n.kde.org/docs/translation-howto/)

## Utilidades
* Buscar texto en los proyectos:
    * Traducciones: https://l10n.kde.org/dictionary/search-translations.php
    * Source Code: https://lxr.kde.org/search

# Webs
## Estructura del repo
* Autogenerado por hugoi18n al compilar los ficheros .po, aunque realmente hay un proceso que lo hace desde SVN
    * content-trans/*
    * i18n/*

* En las webs realmente no hay que usar [hugoi18n](https://invent.kde.org/websites/hugo-i18n), a no ser que quieras probar con un laboratorio en local. Estará en el repo para su uso por gitlab.

## Ejemplo
* https://eco.kde.org
* [gitlab](https://invent.kde.org/websites/eco-kde-org)
* Ruta de las traducciones (.po) en SVN
    * https://websvn.kde.org/trunk/l10n-kf5/es/messages/websites-eco-kde-org/
    * https://websvn.kde.org/trunk/l10n-kf5/es/messages/websites-aether-sass/
* Activar la sincronización SVN-Gitlab
    * https://invent.kde.org/sysadmin/repo-metadata/-/blob/master/projects-invent/websites/eco-kde-org/i18n.json
* Stats
    * https://l10n.kde.org/stats/gui/trunk-kf5/team/es/websites-eco-kde-org/

## Laboratorio
* Preparar web para ver como queda la traducción
```bash
cd ~/git/invent.kde.org/
git clone git@invent.kde.org:websites/aether-sass.git -b hugo --single-branch
# aether-sass es un requisito de casi todas las webs KDE
```

* Preparar entorno python con hugo-i18n
```bash
cd ~/git/invent.kde.org/
git clone git@invent.kde.org:websites/hugo-i18n.git
python3 -m venv eco-py      # Crearmos entorno virtual
source eco-py/bin/activate  # Lo activamos
cd hugo-i18n
python -m pip install -r requirements.txt   # Instalamos dependencias (opcional)
pip install . # Instalamos hugo-i18n (instalará dependencias si faltan)
```

* Generar traducción
```bash
source ~/git/invent.kde.org/eco-py/bin/activate  # Activamos venv python
cd ~/git/invent.kde.org/eco-kde-org
mkdir locale.es
# wget https://websvn.kde.org/*checkout*/trunk/l10n-kf5/es/messages/websites-eco-kde-org/eco-kde-org_www.po
cp ~/git/invent.kde.org/svn/eco-kde-org_www.po locale.es/es
export PACKAGE=websites-eco-kde-org
export FILENAME="eco-kde-org_www"
export LANG=en_US.UTF-8
hugoi18n compile locale.es # Genera carpeta local con .mo
hugoi18n generate   # Genera el i18n y content-trans
hugo server --i18n-warnings
```
