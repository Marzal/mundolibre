---
title: "Gitlab"
icon: fa-brands fa-gitlab
linkTitle: "Gitlab"
weight: 1
date: 2021-07-27
description: >
  Alternativa más libre a Github
categories: [Web]
tags: [SSG,JAMStack,DIY]
---
## Git global setup
```bash
git config --global user.name "David Marzal"
git config --global user.email "******-Username@users.noreply.gitlab.com"
```

## Badges

* [Oficiales](https://docs.gitlab.com/ee/user/project/badges.html)
    * gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg -> gitlab.com/%{project_path}/-/commits/%{default_branch}
    *  gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg
* [Badgen](https://badgen.net/gitlab)
    * badgen.net/gitlab/license/%{project_path} -> gitlab.com/%{project_path}
    * badgen.net/gitlab/last-commit/%{project_path} -> gitlab.com/%{project_path}/-/commits
    * badgen.net/gitlab/branches/%{project_path} -> gitlab.com/residuocerorm/{project_path}/-/branches
    * No van
        * badgen.net/gitlab/commits/%{project_path} -> gitlab.com/%{project_path}/-/commits
* [Shields.io](https://shields.io/)
    * 
* Ejemplos
    * <https://gitlab.com/ar-/apple-flinger>
