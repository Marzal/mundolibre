---
title: "OpenWrt"
icon: fa-solid fa-tower-broadcast
linkTitle: "OpenWrt"
weight: 20
date: 2021-07-17
description: >
  Routers con firmware libre
categories: [Web]
tags: [privacidad,IOT,DIY]
---

# General
* [Web oficial](https://openwrt.org/)

## Primeros pasos

```bash
cat /etc/config/network
df -hT
opkg install diffutils

```

### Luci WEB
* [Instalación](https://openwrt.org/docs/guide-user/luci/luci.essentials)
```bash
opkg update
opkg install luci
#opkg install luci-ssl  # Si queremos acceso con cifrado
/etc/init.d/uhttpd restart
opkg list luci-i18n-\* | grep -i spanish
```
* Aplicaciones
```bash
opkg list luci-app-\* # Listar aplicaciones disponibles

# https://openwrt.org/docs/guide-user/luci/luci_app_statistics
opkg install luci-app-statistics
opkg list | grep collectd-mod
/etc/init.d/luci_statistics enable
/etc/init.d/collectd enable
```

## Paquetes
* https://openwrt.org/packages/start
* [opkg](https://openwrt.org/docs/guide-user/additional-software/opkg)
opkg update
* [extras](https://openwrt.org/docs/guide-user/advanced/opkg_extras)
```bash
opkg list-installed
opkg list-upgradable
```
* conf
```
dest root /
dest ram /tmp
lists_dir ext /var/opkg-lists
option overlay_root /overlay
```

## Sistema
* [Command-line interpreter](https://openwrt.org/docs/guide-user/base-system/user.beginner.cli)

### UCI
https://openwrt.org/docs/guide-user/base-system/uci

```bash
uci show
uci show system
```
### extroot
https://openwrt.org/docs/guide-user/additional-software/extroot_configuration

```bash
opkg list-installed | grep f2fs
```

### Actualizar
* https://openwrt.org/docs/guide-user/installation/generic.sysupgrade
* [Mediante ssh](https://openwrt.org/docs/guide-user/installation/sysupgrade.cli)
```bash
opkg list-changed-conffiles
find /lib/upgrade/keep.d/
cat /etc/sysupgrade.conf

#Post-upgrade
find /etc -name *-opkg
```

## Docu
* https://openwrt.org/docs/start

## Aplicaciones

### dnscrypt-proxy2
* [Configuración](https://openwrt.org/docs/guide-user/services/dns/dnscrypt_dnsmasq_dnscrypt-proxy2)

* viejo
```bash
opkg files dnscrypt-proxy
Package dnscrypt-proxy (2018-11-22-f61ca76a-1) is installed on root and has the following files:
/etc/config/dnscrypt-proxy
/usr/sbin/dnscrypt-proxy
/etc/init.d/dnscrypt-proxy


opkg files dnscrypt-proxy-resolvers
Package dnscrypt-proxy-resolvers (2018-11-22-f61ca76a-1) is installed on root and has the following files:
/usr/share/dnscrypt-proxy/dnscrypt-resolvers.csv
```

### AdguardHome

* GL [foro](https://forum.gl-inet.com/t/adguardhome-on-gl-routers/10664)
    * beryl necesita `gl-agh-stats` y extrootfs

## Marcas
* [Con OpenWrt de serie](https://openwrt.org/docs/guide-user/installation/openwrt-as-stock-firmware)

### GL iNet
* [Beryl (GL-MT1300)](https://www.gl-inet.com/products/gl-mt1300/)
    * Info
        * arch: ramips/mt7621
        * SW : 3.201 - 2021-04-02 20:10:38 !!Es una mezcla de OpenWrt y GL
        * Usa drivers propietarios para el WiFi
        * [Paquetes por defecto](https://forum.gl-inet.com/t/beryl-gl-mt1300-request-for-list-of-originally-installed-packages/14403/2)
    * [Setup/doc](https://docs.gl-inet.com/en/3/setup/gl-mt1300/first-time_setup/)
    * [LED](https://forum.gl-inet.com/t/gl-mt1300-led-control-instructions/13338)
    * [OpenWrt hwdata](https://openwrt.org/toh/hwdata/gl.inet/gl.inet_gl-mt1300)
        * BusyBox v1.30.1 () built-in shell (ash)
        * OpenWrt 19.07.7, r11306-c4a6851c72
