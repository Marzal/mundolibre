---
title: "DNS"
icon: fa-solid fa-network-wired
linkTitle: "DNS"
weight: 5
date: 2021-06-26
description: >
  Resolución de nombres y cifrado
categories: [Web]
tags: [dns,privacidad]
---
https://wiki.archlinux.org/index.php/Domain_name_resolution

## DNS
* Utilidades
    * El paquete __bind__ proporciona `dig`, `host`, `nslookup`
    * __ldns__ proporciona `drill`

## ECH
 * <https://blog.mozilla.org/security/2021/01/07/encrypted-client-hello-the-future-of-esni-in-firefox/>
 * <https://img1.wsimg.com/blobby/go/b5eb0792-7081-48f3-b13f-1f464bf1f8a1/Notes%20From%20A%20Roundtable%20Discussion%20About%20-0001.pdf>
### Config
* [dnscrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy/wiki/Local-DoH)
    * Comprobar resolver : `journalctl --no-pager -b -u dnscrypt-proxy.service`

* [firefox](https://wiki.mozilla.org/Trusted_Recursive_Resolver) : <about:config>
    * `network.trr.mode` : 3
    * `network.trr.uri` : https://127.0.0.1:3000/dns-query
    * `network.trr.custom_uri` : https://127.0.0.1:3000/dns-query
    * ECH
        * `network.dns.echconfig.enabled` : true
        * `network.dns.use_https_rr_as_altsvc` : true
    * ESNI (Obsoleto)
        * `network.security.esni.enabled` : true
    * https://bugzilla.mozilla.org/show_bug.cgi?id=1667801

### Comprobaciones
* <https://www.cloudflare.com/es-es/ssl/encrypted-sni/> - No funciona con ECH
    * <https://www.cloudflare.com/cdn-cgi/trace>
* <https://clienttest.ssllabs.com:8443/ssltest/viewMyClient.html>
* <https://esnjcheck.com>
* <https://servo.org/cdn-cgi/trace>
