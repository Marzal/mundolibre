---
title: "Datos Abiertos"
icon: fa-brands fa-creative-commons
linkTitle: "OpenData"
weight: 8
date: 2021-01-25
description: >
  Información, aplicaciones y servicios que usan datos abiertos
categories: [Web]
tags: [OpenData,Sostenibilidad]
---
Los datos abiertos NO son lo mismo que el software abierto, pero son filosofías hermanas.

## Catalogos y Aplicaciones
### Gobierno de España
* <https://datos.gob.es/es/catalogo?theme_id=medio-ambiente>
* <https://datos.gob.es/es/aplicaciones>
    * <https://datos.gob.es/es/aplicaciones/punto-limpio>
    * <https://datos.gob.es/es/aplicaciones/aemet-cli> - [Código](https://github.com/davidpob99/aemet-cli)
* <http://www.aemet.es/es/datos_abiertos>

### Instituciones
* Univerisades
    * <https://www.universidata.es/>
* Internacionales
    * [Portal de datos abiertos de la UE](https://es.wikipedia.org/wiki/Portal_de_datos_abiertos_de_la_UE) - [Otro portal](https://www.europeandataportal.eu/es)
    * [NASA](https://earthdata.nasa.gov/)

### Empresas
* <https://opendata.esri.es>

## Servicios FLOSS

* [OpenStreetMaps](https://www.openstreetmap.org) - [FOSS](https://wiki.osmfoundation.org/wiki/FOSS_Policy_Committee)
    * [Humanitarian OpenStreetMap](https://www.hotosm.org/)
* [WikiMedia](https://www.wikimedia.es/) - [Código](https://wikimediafoundation.org/technology/)
    * [Wikipedia](https://es.wikipedia.org/wiki/Datos_abiertos)
    * [WikiCommons](https://commons.wikimedia.org/wiki/Category:Open_data)
    * [WikiData](https://www.wikidata.org/wiki/Q309901) - [Query Service](https://query.wikidata.org/)
* [OpenFoodFacts](https://es.openfoodfacts.org/) - [Wiki](https://en.wikipedia.org/wiki/Open_Food_Facts)
    * [Código AGPL3/Apache](https://github.com/openfoodfacts)
    * [Open Beauty Facts](https://github.com/openfoodfacts/openbeautyfacts)

## Eventos
* [Open Data Day](https://opendataday.org/)
    * [Día de los Datos Abiertos 2021 - A Coruña](https://odd2021acoruna.gitlab.io/)
    * [Charla: Datos abiertos, SL y medio ambiente](https://devtube.dev-wiki.de/videos/watch/7948bbf6-6967-426a-a390-078619e6d195)

## Medio Ambiente
* [Uso de los datos abiertos en el medio ambiente según el Gobierno de España](https://datos.gob.es/es/sector/medio-ambiente)
    * [Los principales conjuntos de datos de medio ambiente de datos.gob.es](https://datos.gob.es/es/noticia/los-principales-conjuntos-de-datos-de-medio-ambiente-de-datosgobes)
        * Ecologia, conservación de la naturaleza, parques naciones, especies invasoras, indicadores de sostenibilidad, residuos, contenedores, [puntos limpios](https://datos.gob.es/es/aplicaciones/cleanspot-tu-punto-limpio-mas-cercano), [calidad de aire](https://datos.gob.es/es/aplicaciones/indice-de-calidad-del-aire-nacional), espacios naturales, [incendios](https://datos.gob.es/es/aplicaciones/mapa-web-de-incendios-en-tiempo-real), [placas solares](https://datos.gob.es/es/aplicaciones/solarmap)
    * [Casos de uso](https://datos.gob.es/es/blog/casos-de-uso-de-datos-abiertos-para-cuidar-el-medio-ambiente-y-luchar-contra-el-cambio)
    * [Open Data Clímatico](http://www.aemet.es/es/noticias/2019/03/Efectos_del_cambio_climatico_en_espanha)
* Internacionales
    * [Datos abiertos de la UE sobre Medioambiente](https://data.europa.eu/euodp/es/data/publisher/env)
        * [Photovoltaic Geographical Information System (PVGIS)](https://ec.europa.eu/jrc/en/pvgis)
    * La NASA publica una infinidad de datos recogidos por sus satelites y sensores, muchos de ellos enfocados al [clima y medioambiente](https://search.earthdata.nasa.gov/search?fst0=Climate%20Indicators&fst1=Environmental%20Advisories)
    * https://resourcewatch.org/
    * https://www.wri.org/
    * [Earth observation data](https://www.geoportal.org/)
    * [Global Forest Watch](https://www.globalforestwatch.org/) - Forest cover/loss data
* Recomendaciones GeoInnova / Actualidad y Empleo Ambiental
    * [Biodeversidad](https://www.gbif.org/es/what-is-gbif)
    * [Clima](https://worldclim.org)
    * [The power of Google Earth Engine without coding](https://earthmap.org)
    * [Mapas leaflet-providers](https://www.podcastidae.com/programa/cabo-cope-gestion-compra-y-retracto-de-esta-joya-murciana-con-maria-gimenez/)