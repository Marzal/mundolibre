---
title: "SEO"
icon: fa-solid fa-bullhorn
linkTitle: "SEO"
weight: 10
date: 2021-08-12
description: >
  Recursos de posicionamiento
categories: [Web]
tags: [DIY]
---
* Meta tags
    * https://css-tricks.com/essential-meta-tags-social-media/
    * Validadores de "tarjetas"
        * https://developers.facebook.com/tools/debug/
        * https://cards-dev.twitter.com/validator
    * Generador de imagenes
        * https://github.com/chrisvxd/og-impact