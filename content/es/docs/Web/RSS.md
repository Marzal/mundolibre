---
title: "Feed RSS en Hugo"
icon: fa-solid fa-square-rss
linkTitle: "Feed RSS"
weight: 3
date: 2020-12-27
description: >
  Como crear un feed para podcast con HUGO
categories: [web]
tags: [podcast,feed,SSG]
---
## Validators / Validadores
* Entienden Podcasting 2.0
    * https://validator.livewire.io/
    * https://podba.se/validate/
    * https://castfeedvalidator.com
* NO entienden Podcasting 2.0
    * https://validator.w3.org/feed/
        * https://www.rssboard.org/rss-validator/ - Desactualizado
        * http://www.feedvalidator.org/ - Desactualizado

## Namespaces
* Soportados por [AntennaPod](https://github.com/AntennaPod/AntennaPod/tree/develop/parser/feed/src/main/java/de/danoeh/antennapod/parser/feed/namespace):
    * __RSS2__: channel item guid title link description pubDate enclosure {_url,length,type_} image {_url_} [language](https://www.rssboard.org/rss-language-codes)
    * __atom__: ... link {_href,rel,type,title,length_} ...
    * __content__: encoded -> Para contenido con HTML si "description" no es más largo
    * __itunes__: image {_href_} author duration subtitle summary new-feed-url
    * __podcast__ : [funding](https://github.com/AntennaPod/AntennaPod/blob/develop/parser/feed/src/main/java/de/danoeh/antennapod/parser/feed/namespace/PodcastIndex.java) chapters : https://podcastindex.org/namespace/1.0
    * __dc__: date
    * __media__: content url fileSize type duration isDefault medium {_image,audio,video_} thumbnail {_url_} description {_type_} : http://search.yahoo.com/mrss/
    * __simple-chapters__: chapters {_chapter_} {_start,title,href,image_} : http://podlove.org/simple-chapters

### En uso
* <rss version="2.0" ...
    * https://www.rssboard.org/rss-profile
        * For best support in the widest number of aggregators, an __item__ should not contain more than __one enclosure__.
    * https://www.rssboard.org/rss-specification
    * description : Para canal e item. Admite HTML.

* xmlns:atom="http://www.w3.org/2005/Atom
    * https://tools.ietf.org/html/rfc4287#section-4.2.7
    * [Indica](https://www.rssboard.org/rss-profile#namespace-elements-atom) al aggregador cual es la ruta del feed
    ```xml
    <atom:link href="https://ruta.FQDN.alfeed" rel="self" type="application/rss+xml"/>
    ```

* xmlns:content="http://purl.org/rss/1.0/modules/content/"
    * https://web.resource.org/rss/1.0/modules/content/
    * En [AntennaPod](https://github.com/AntennaPod/AntennaPod/blob/master/app/src/androidTest/java/de/test/antennapod/feed/FeedItemTest.java), si __description__ es más largo es preferido.
    * [content:encoded](https://www.rssboard.org/rss-profile#namespace-elements-content) está diseñado para poner el contenido completo del árticulo en HTML, no como el _description_ que puede ser un resumen.
    ```xml
    <content:encoded>
    <![CDATA[%s]]>
    </content:encoded>
    ```

* xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" 
    * Depende de __content__
    * No le gustan los números en los titulos de los items, prefiere usar. Pero da igual.
        ```xml
        <itunes:title>
        <itunes:episode>
        <itunes:season>
        ```
    * `<itunes:summary>` : Limited to 4000 characters or less, plain text, no HTML
        * No es necesario, se puede usar _description_
    * `<itunes:subtitle>` : Limited to 255 characters or less, plain text, no HTML
        * No es necesario.

* xmlns:podcast="https://podcastindex.org/namespace/1.0"
    * Documentado en la [plantilla/generador](https://gitlab.com/Marzal/hugo-cards-podcast#podcasting-20)

### Sin uso
* xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    * https://web.resource.org/rss/1.0/modules/syndication/
    ```xml
    <sy:updatePeriod> ( 'hourly' | 'daily' | 'weekly' | 'monthly' | 'yearly' )
    <sy:updateFrequency> ( a positive integer )
    <sy:updateBase> ( #PCDATA ) [W3CDTF]
    ```

* xmlns:dc="http://purl.org/dc/elements/1.1/"
    * https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#section-3
    * http://www.hipertexto.info/documentos/dublin_core.htm
    ```xml
    <dc:date>2020-12-26</dc:date> || <dc:date>2020-12-26T01:30:00Z</dc:date>
    <dc:creator>Menos restrictivo que author e incomatible con el</dc:creator>
    ```

* xmlns:rawvoice="http://www.rawvoice.com/rawvoiceRssModule/"
    * https://rawvoice.com/services/tools-and-resources/rawvoice-rss-2-0-module-xmlns-namespace-rss2/
    * When the __RawVoice RSS Module__ is present, it is assumed that there is only one `<enclosure>` per `<item>`
    * No "compatible" con __Yahoo Media__
    ```xml
    <rawvoice:frequency>Weekly</rawvoice:frequency>
    <rawvoice:location>Spain, Europe</rawvoice:location>
    <rawvoice:rating>TV-Y</rawvoice:rating>
    ```

* xmlns:media="http://search.yahoo.com/mrss/"
    * https://www.rssboard.org/media-rss
    * https://www.feedforall.com/mediarss.htm

* http://podlove.org/simple-chapters
    * https://github.com/podlove/podlove-specifications/blob/master/podlove-simple-chapters.md
    * item
    ```xml
    <psc:chapters version="1.2" xmlns:psc="http://podlove.org/simple-chapters">
        <psc:chapter start="0" title="Welcome" />
        <psc:chapter start="3:07" title="Introducing Podlove" href="http://podlove.org/" />
        <psc:chapter start="12:42" title="Resumée" />
    </psc:chapters>
    ```
    * channel
    ```xml
    <atom:link rel="http://podlove.org/simple-chapters" href="http://podlove.org/examples/chapters.psc" />
    ```

* xmlns:creativeCommons="http://backend.userland.com/creativeCommonsRssModule"
    ```xml
    <creativeCommons:license>
        http://www.creativecommons.org/licenses/by-sa/4.0
    </creativeCommons:license>
    ```

* xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    * https://developer.mozilla.org/en-US/docs/Archive/RSS/Article/Why_Well-Formed_Web_RSS_Module_is_Popular_-_Syndicating_Your_Comments
    ```xml
    <wfw:commentRss>http://www.example.com/feed/rss/54320/comments</wfw:commentRss>
    ```

* xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    * https://web.resource.org/rss/1.0/modules/slash/
    ```xml
    <slash:section> ( #PCDATA ) </slash:section>
    <slash:department> ( #PCDATA ) </slash:department>
    <slash:comments> ( positive integer ) </slash:comments>
    <slash:hit_parade> ( comma-separated integers )</slash:hit_parade>
    ```

### Docu
* https://support.google.com/podcast-publishers/answer/9889544
* https://viapodcast.fm/cinco-preguntas-clave-para-entender-que-es-un-feed-rss/
* https://blog.forret.com/2005/06/20/rss-with-images-picture-podcasting/
* https://github.com/gpodder/podcast-feed-best-practice/blob/master/podcast-feed-best-practice.md
* https://github.com/Podcast-Standards-Project/PSP-1-Podcast-RSS-Specification
* Itunes
    * https://help.apple.com/itc/podcasts_connect/#/itcb54353390
    * https://www.drupal.org/docs/7/modules/advanced-views-rss-feed/setting-up-itunes-podcast-feed-elements
    * https://itunespartner.apple.com/podcasts/articles/podcast-requirements-3058
    * https://podnews.net/article/html-episode-notes-in-podcast-rss
    * https://www.feedforall.com/itune-tutorial-tags.htm#category

## Hugo

### Generador con hugo
* Plantilla para generar un feed en Hugo, pensada para [generarse desde un section](https://gitlab.com/Marzal/hugo-cards-podcast)

{{< tabpane right=false >}}
  {{% tab header="**Feed**:" disabled=true /%}}
  {{% tab header="-->" lang="es" text=true %}}
Contenido escondido en la pestaña RSS para ahorrar espacio. Tambien revisable [aquí](https://gitlab.com/Marzal/hugo-cards-podcast/-/blob/main/layouts/section/rss.xml?ref_type=heads)
  {{% /tab %}}
  {{< tab header="rss.xml" lang="xml" >}}
{{- printf "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>" | safeHTML }}
<rss version="2.0"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
    xmlns:podcast="https://podcastindex.org/namespace/1.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
>
<channel>
    <title>{{ with .Title }}{{.}}{{ end }}</title>
    <link>{{ .Permalink }}</link>
    {{ with .Description }}
    <description>{{.}}</description> {{ end }}
    <generator>https://gitlab.com/Marzal/hugo-cards-podcast</generator>
    {{ with .Site.LanguageCode }}<language>{{.}}</language>{{ end }}
    {{ if not .Date.IsZero }}
    <lastBuildDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 +0000" | safeHTML }}</lastBuildDate> {{ end }}
    {{ with .OutputFormats.Get "RSS" }}
        {{ printf "<atom:link href=%q rel=\"self\" type=%q />" .Permalink .MediaType | safeHTML }} {{ end }}
    {{ with .Site.Author.email }}
        <managingEditor>{{.}} {{ with $.Site.Author.name }}({{.}}){{ end }}</managingEditor>
        <webMaster>{{.}}{{ with $.Site.Params.builtBy }} ({{.}}) {{- else }} ({{ $.Site.Author.name}}){{ end }}</webMaster>
    {{ end }}
    {{ with .Site.Author.name }}
    <copyright>{{.}} {{ now.Format "2006" }}</copyright> {{ end }}
    {{ if .Site.Params.copyright.license }}
    <podcast:license {{ with .Site.Params.copyright.licenseURL }}url="{{.}}"{{ end }}>({{.Site.Params.copyright.license}})</podcast:license>
    {{ end }}
    {{ with .Params.p20.guid }}
    <podcast:guid>{{.}}</podcast:guid>{{ end }}
    {{ if .Param "p20.fundingURL" }}
    <podcast:funding {{ with .Param "p20.fundingURL" }}url="{{.}}"{{ end }}>{{ .Param "p20.funding" }}</podcast:funding>
    {{ end }}
    <podcast:locked {{ with .Site.Author.email }}owner="{{.}}"{{ end }}>{{ with .Param "p20.locked" -}}{{.}}{{- else -}} no {{- end }}</podcast:locked>
    {{ if .Param "p20.location" }}
    <podcast:location 
    {{ with .Param "p20.locationGEO" }}geo="geo:{{.}}"{{ end }}
    {{ with .Param "p20.locationOSM" }}osm="{{.}}"{{ end }}
    >{{.Param "p20.location"}}</podcast:location>
    {{ end }}
    <image>
        <url>{{ .CurrentSection.Permalink }}{{ .Params.podcast_image}}</url>
        {{ with .Title }}<title>{{.}}</title>{{ end }}
        <link>{{ .Permalink }}</link>
    </image>
    <itunes:explicit>{{ with .Param "itunes.explicit" -}}{{.}}{{- else -}} false {{- end }}</itunes:explicit>
    <itunes:category text="Technology" />
    <itunes:category text="News">
        <itunes:category text="Tech News" />
    </itunes:category>
    <itunes:image href="{{ .CurrentSection.Permalink }}{{ .Params.podcast_image}}" />
    {{ with .Site.Author.name }}
        <itunes:author>{{.}}</itunes:author>
        <itunes:owner>
            <itunes:name>{{.}}</itunes:name> {{ end }}
    {{ with .Site.Author.email }}<itunes:email>{{.}}</itunes:email>
        </itunes:owner>
    {{ end }}
    {{ range $i, $pg := .Pages }}
    <item>
        <title>{{ $pg.Params.title }}</title>
        <link>{{ .Permalink }}</link>
        <pubDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 GMT" | safeHTML }}</pubDate>
        <guid isPermaLink="false">{{ $pg.Params.title }}</guid> 
            {{ with .Params.podcast.mp3 }}
                {{ $file_stat := os.Stat (add "/static/images/" (. | relURL)) }}<enclosure url="{{ . | absURL }}" length="{{ $file_stat.Size }}" type="image/png" />
            {{ end }}
        {{ if .Params.podcast.audio }}
        <enclosure url="{{.Params.podcast.audio}}.ogg" length="{{ with .Params.podcast.olength -}} {{.}} {{- else -}} 0 {{- end }}" type="audio/ogg" />
        <enclosure url="{{.Params.podcast.audio}}.mp3" length="{{ with .Params.podcast.mlength -}} {{.}} {{- else -}} 0 {{- end }}" type="audio/mpeg" />
            {{ with .Params.podcast.iduration }}
        <itunes:duration>{{.}}</itunes:duration>
            {{ end }}
        {{ end }}
        {{ printf "<description><![CDATA[%s]]></description>" .Summary | safeHTML }}
        {{ printf "<content:encoded><![CDATA[%s]]></content:encoded>" .Content | safeHTML }}
        {{ with .Site.Author.email }}
            <author>{{.}}{{ with $.Site.Author.name }} ({{.}}){{ end }}</author>
        {{ end }}
        <itunes:explicit>{{ with .Param "itunes.explicit" -}}{{.}}{{- else -}} false {{- end }}</itunes:explicit>
        <itunes:image href="{{.Site.BaseURL}}images/{{.Params.img}}"></itunes:image>
        {{ with .Site.Author.name }}<itunes:author>{{.}}</itunes:author>{{ end }}
        {{ if and (.CurrentSection.Params.p20.protocol) (.Params.podcast.siuri) }}
        <podcast:socialInteract
            uri="{{ .Params.podcast.siuri }}"
            protocol="{{ .CurrentSection.Params.p20.protocol }}"
            {{ with .CurrentSection.Params.p20.accountId }}accountId="{{.}}"{{ end }}
            {{ with .CurrentSection.Params.p20.accountUrl }}accountUrl="{{.}}"{{ end }}
        /> {{ end }}
        {{ if and (.Params.podcast.trurl) (.Param "p20.trtype") }}
        <podcast:transcript url="{{ .Params.podcast.trurl }}" type="{{ .Param "p20.trtype" }}"
        />{{ end }}
        {{ if .Params.podcast.location }}
        <podcast:location 
        {{ with .Params.podcast.locationGEO }}geo="geo:{{.}}"{{ end }}
        {{ with .Params.podcast.locationOSM }}osm="{{.}}"{{ end }}
        >{{.Params.podcast.location}}</podcast:location>
        {{ end }}

    </item>
    {{ end }}
</channel>
</rss>
  {{< /tab >}}
{{< /tabpane >}}

### Duración del audio
* [Fijo a 0](https://www.rssboard.org/rss-profile#element-channel-item-enclosure)
```Go
    <enclosure url="{{ $pg.Params.link }}" type="audio/mpeg" length="0" />
```
* Sacandolo del Params
```Go
    {{ $audio_name_arr := split (index .Params.audio 0) "." }}
    {{ $audio_type := index $audio_name_arr (sub (len $audio_name_arr) 1) }}
    {{ $audio_size := 0 }}
    {{- if .Params.audio_size }}{{ $audio_size = int .Params.audio_size }} {{ end -}}
    <enclosure url="{{ index .Params.audio 0 | absURL }}" length="{{ $audio_size }}" type="audio/{{ $audio_type }}" />
```

```Go
    <enclosure url="{{ .Site.Params.media_prefix }}{{ .Params.podcast_file }}" {{with .Params.podcast_bytes}}length="{{ . }}"{{ end }} type="audio/mpeg" />
```

* RSS
    * https://gohugo.io/templates/rss
    * https://support.google.com/news/publisher-center/answer/9545420?hl=en
    * https://developer.mozilla.org/en-US/docs/Archive/RSS/Article/Why_RSS_Content_Module_is_Popular_-_Including_HTML_Contents

### Ejemplos
* https://github.com/frjo/hugo-theme-zen/blob/master/layouts/podcast/rss.xml
* https://github.com/funkydan2/alpha-church/blob/master/layouts/sermons/sermons.rss.xml
* https://github.com/mattstratton/castanet/blob/master/layouts/section/episode.rss.xml

### Foros
* Feed para podcast
    * https://discourse.gohugo.io/t/need-to-create-a-podcast-friendly-rss-feed/1727/16
    * https://discourse.gohugo.io/t/help-generating-rss-xml-for-podcast/18516
* Duración de episodios
    * https://discourse.gohugo.io/t/migrate-feed-podcast-jekyll-to-hugo/20115/7
* Dos feeds
    * https://discourse.gohugo.io/t/migrate-jekyll-podcast-to-hugo/25937/7
* Redirect RSS
    * https://gohugo.io/content-management/urls
    * https://discourse.gohugo.io/t/how-can-i-change-the-rss-url/118/7