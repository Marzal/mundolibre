---
title: "HUGO"
icon: fa-brands fa-golang
linkTitle: "HUGO"
weight: 2
date: 2021-01-01
description: >
  Generador de páginas estáticas
categories: [Web]
tags: [SSG,JAMStack]
---
* Info
  * https://gohugo.io/getting-started
* Config básica
  * https://gohugo.io/getting-started/configuration

## Formato
* Eliminar espacio en blanco al usar shortcodes
  * https://gohugo.io/templates/introduction/#whitespace

### Colorear código
* Usa [Chroma](https://github.com/alecthomas/chroma#supported-languages), en su web se pueden ver los lenguajes soportados.
* https://gohugo.io/content-management/syntax-highlighting/
  * https://bwaycer.github.io/hugo_tutorial.hugo/extras/highlighting/

### Fechas
* dateformat: https://gohugohq.com/howto/hugo-dateformat/

### CSS
* CSS: https://discourse.gohugo.io/t/how-to-override-css-classes-with-hugo/3033/8
* CSSS
  * https://gohugo.io/getting-started/configuration/#configure-build
  * https://blog.fullstackdigital.com/how-to-use-hugo-template-variables-in-scss-files-in-2018-b8a834accce
  * Prune
    * https://gohugo.io/hugo-pipes/postprocess/#css-purging-with-postcss
  * [resources](https://github.com/gohugoio/hugoThemes#resources) : La carpeta resources es necesaria si no se usa hugo-extended

### Layout
* Paginación
  * https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

## Linea de comandos
* Para probar con todo el contenido futuro o caducado
```Bash
hugo server -DEF --gc --i18n-warnings --debug

-D, --buildDrafts -> include content marked as draft
-E, --buildExpired -> include expired content
-F, --buildFuture -> include content with publishdate in the future
--gc -> enable to run some cleanup tasks (remove unused cache files) after the build
--i18n-warnings -> print missing translations
-e production
```

## Gitlab Pages
* https://gitlab.com/pages/hugo/blob/registry/Dockerfile

* Usar gzip con Hugo
  * Docu: [1](https://webd97.de/post/gitlab-pages-compression/) [2](https://webmasters.stackexchange.com/questions/119670/how-do-you-serve-jekyll-pages-with-gzip-compression-on-gitlab-pages)
  * Comando y para [comprobarlo](https://www.whatsmyip.org/http-compression-test)
`find public \( -name '*.html' -o -name '*.css' -o -name '*.js' \) -exec gzip -fk {} +`

* Yarn
  * Instalacion en imagenes de contenedores docker
    * Instalar en alpine `apk add yarn`
      * https://yarnpkg.com/getting-started/qa#which-files-should-be-gitignored
    * V1 : https://classic.yarnpkg.com/en/docs/install-ci/
      * Luego se puede actualizar a berry (v2)
  * Comparación con npm : [yarn > speed](https://www.whitesourcesoftware.com/free-developer-tools/blog/npm-vs-yarn-which-should-you-choose/)
      
### Articulos
* Inicial
  * <https://loadbalancing.xyz/post/blogging-with-hugo-and-gitlab-quick-start/>
  * <https://loadbalancing.xyz/post/blogging-with-hugo-and-gitlab-local-development-setup>
  * <https://elblogdelazaro.gitlab.io/2019-09-16-crea-tu-blog-con-hugo-y-gitlab-pages/>
* Medio
  * <https://elblogdelazaro.gitlab.io/post/2019-10-07-script-para-programar-artículos-en-gitlab-pages>
* Avanzado
  * <https://loadbalancing.xyz/post/blogging-with-hugo-and-gitlab-custom-domains/>
  * <https://loadbalancing.xyz/post/blogging-with-hugo-and-gitlab-https-with-letsencrypt/>
  * <https://elblogdelazaro.gitlab.io/post/2018-02-01-gitlab-pages-con-tls-y-letsencrypt>
  * <https://loadbalancing.xyz/post/blogging-with-hugo-and-gitlab-letsencrypt-auto-renewal/>


## Temas
* Repositorio oficial -> https://github.com/gohugoio/hugoThemes
  * https://themes.gohugo.io/syna/
  * https://themes.gohugo.io/bootstrap-bp-hugo-startpage/
  * https://themes.gohugo.io/hugo-theme-doors/
  * https://themes.gohugo.io/parsa-hugo-personal-blog-theme/
  * https://themes.gohugo.io/meghna-hugo/
  * https://themes.gohugo.io/forty/
  * https://themes.gohugo.io/hugo-theme-dopetrope/

  * https://themes.gohugo.io/hugo-theme-w3css-basic/
  * https://themes.gohugo.io/theme/slate/
  * Documentación
    * https://github.com/alex-shpak/hugo-book

### Syna

* Instalación
  * [Añadir a proyecto existente](https://about.okkur.org/syna/docs/installation/)
  ```bash
  git submodule init # If you haven't initialized before
  git submodule add https://github.com/okkur/syna.git themes/syna
  cd themes/syna
  git checkout v0.17 # Latest release as of now is v0.17.0
  ```
  * Empezar proyecto desde 0 con plantilla
  ```bash
  git clone https://github.com/okkur/syna-start && cd syna-start
  git submodule init
  git submodule update
  ```
* Documentación
  * https://syna-demo.okkur.org/

#### CSS
* Resources
  * https://github.com/okkur/syna-start/issues/35
* PurgeCSS con PostCSS [1](https://github.com/okkur/syna/pull/797)
  * /postcss.config.js
  * /config.toml : `[build] writeStats = true`

#### Errores
* Failed to read Git log: Git executable not found in $PATH [1](https://discourse.gohugo.io/t/problem-with-enablegitinfo-on-gitlab-com-pages/20354)
  * `#enableGitInfo` o `apk add git`

### Docsy
https://www.docsy.dev/docs/

* [Requisitos](https://www.docsy.dev/docs/getting-started/)
  * hugo-extended
  * postcss postcss-cli autoprefixer
* [Actualizar](https://www.docsy.dev/docs/updating/)
  * Como submodulo
    * `git submodule update --remote; git add themes/ ; ` 
    * `git commit -m "Updating theme submodule" ; git push origin master`


## SEO
* https://buttercms.com/blog/a-complete-dead-simple-guide-to-seo-for-static-site-generators
* https://buttercms.com/blog/creating-a-static-website-with-hugo-and-buttercms
* https://keithpblog.org/post/hugo-website-seo/
