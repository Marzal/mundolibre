# MundoLibre

Recopilación de notas y apuntes sobre software libre.

Disponible en https://marzal.gitlab.io/mundolibre

# Licencia

All content residing under the "content/" directory of this repository is licensed under "Creative Commons: CC BY-SA 4.0 license".
